<?php
//PROD
$metadata['http://na-kkgfs.kavokerrgroup.com/adfs/services/trust'] = array (
  'entityid' => 'http://na-kkgfs.kavokerrgroup.com/adfs/services/trust',
  'contacts' => 
  array (
    0 => 
    array (
      'contactType' => 'support',
    ),
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://na-kkgfs.kavokerrgroup.com/adfs/ls/',
      'index' => 0,
      'isDefault' => true,
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
      'Location' => 'https://na-kkgfs.kavokerrgroup.com/adfs/ls/',
      'index' => 1,
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://na-kkgfs.kavokerrgroup.com/adfs/ls/',
      'index' => 2,
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://na-kkgfs.kavokerrgroup.com/adfs/ls/',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://na-kkgfs.kavokerrgroup.com/adfs/ls/',
    ),
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIGnDCCBYSgAwIBAgITHQACLF2k+JO/yfu/9wABAAIsXTANBgkqhkiG9w0BAQsFADBeMRMwEQYKCZImiZPyLGQBGRYDY29tMRwwGgYKCZImiZPyLGQBGRYMc3licm9uZGVudGFsMRMwEQYKCZImiZPyLGQBGRYDc2RzMRQwEgYDVQQDEwtTeWJyb25FbnRDQTAeFw0xNjA2MDkyMjQ2MThaFw0yMDA2MDgyMjQ2MThaMH8xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTESMBAGA1UEBxMJU2FudGEgQW5hMQwwCgYDVQQKEwNLS0cxCzAJBgNVBAsTAklUMTQwMgYDVQQDEytuYS1ra2dmcy5rYXZva2Vycmdyb3VwLmNvbS50b2tlbi5kZWNyeXB0aW5nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtpT8mQH4lNmMMdwf/lRq/1byz5sfrQbUBDkTCr6NTVhEWk5QYm/AU+FZj8ayHq+0pCo0hXQYzCfhg31zIJv2u+JE8iIpzI3GMTu30qP4U/J6i/JVQwQbLenUWjMK2EHs+BW1ZXdve5/+Q3Ca4nJwKSe4ahDwDljaGLeBX6zD5mfuEAi34CTtGpTQLIPg0F4WG6WGI+KBseg9s/0NOJA3Hzwhe7qaAQj3eoW6kR3GQqOwiZoASzx2L2Kg/hu1isDBksNBI+hMAR8ri0eWvnfxpKY9goZNHTi6aunM1DkKdH2Cbw/gjMsowJnKlYeQm9bcXIueWrYJim3Zhsiw/xJeNwIDAQABo4IDMDCCAywwPgYJKwYBBAGCNxUHBDEwLwYnKwYBBAGCNxUIh9qTcYSxgj2G4ZcphsakAIPpxzqBE4Wc/UCCgMJWAgFkAgEXMBMGA1UdJQQMMAoGCCsGAQUFBwMBMAsGA1UdDwQEAwIFoDAbBgkrBgEEAYI3FQoEDjAMMAoGCCsGAQUFBwMBMB0GA1UdDgQWBBTXWcLMcFcrvgR5a2czDdMJwXaCOzAfBgNVHSMEGDAWgBQNdA7U+vC6WSD/E6mfmKg/Ghcw1jCCASQGA1UdHwSCARswggEXMIIBE6CCAQ+gggELhoHDbGRhcDovLy9DTj1TeWJyb25FbnRDQSxDTj1vcmctdm1zc2NhZTEsQ049Q0RQLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9c2RzLERDPXN5YnJvbmRlbnRhbCxEQz1jb20/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1dGlvblBvaW50hkNodHRwOi8vb3JnLXZtc3NjYWUxLnNkcy5zeWJyb25kZW50YWwuY29tL0NlcnRFbnJvbGwvU3licm9uRW50Q0EuY3JsMIIBQQYIKwYBBQUHAQEEggEzMIIBLzCBtgYIKwYBBQUHMAKGgalsZGFwOi8vL0NOPVN5YnJvbkVudENBLENOPUFJQSxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPXNkcyxEQz1zeWJyb25kZW50YWwsREM9Y29tP2NBQ2VydGlmaWNhdGU/YmFzZT9vYmplY3RDbGFzcz1jZXJ0aWZpY2F0aW9uQXV0aG9yaXR5MHQGCCsGAQUFBzAChmhodHRwOi8vb3JnLXZtc3NjYWUxLnNkcy5zeWJyb25kZW50YWwuY29tL0NlcnRFbnJvbGwvb3JnLXZtc3NjYWUxLnNkcy5zeWJyb25kZW50YWwuY29tX1N5YnJvbkVudENBKDEpLmNydDANBgkqhkiG9w0BAQsFAAOCAQEAK58qsMS+efSuPLL4KfQJ2ecAuQgpUvbIoX+O+IUN1fudFy0ohxsHvAriPIkoV/ZlrUdmD0BPLdE03hUJfjb87gP2CB+28RB0nQffqsERA9L7vTYjEzlMBzSqq8lPJMjhTKlBdyCmzCmnJokf++YPR8B3jBg8hzjH6r212roNL239d6107vV53szwQ3FMzxCVd2IUTMu2m2p4QQcPwFcNruzyRayrUoMHLNJzr1vDhBq/dF+s8W/+gHJGfhCTNoIACrCFJ3F0Cwp1Ko8hOBDpQkWhhHOfHG1SoYxUXFQnEDLhY2sC6OqXmQbH30QQ/GSVy8xZ5zT40yu27i3F+b/lfw==',
    ),
    1 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIGmTCCBYGgAwIBAgITHQACLF5jQLZ3WYI+owABAAIsXjANBgkqhkiG9w0BAQsFADBeMRMwEQYKCZImiZPyLGQBGRYDY29tMRwwGgYKCZImiZPyLGQBGRYMc3licm9uZGVudGFsMRMwEQYKCZImiZPyLGQBGRYDc2RzMRQwEgYDVQQDEwtTeWJyb25FbnRDQTAeFw0xNjA2MDkyMjQ3MDZaFw0yMDA2MDgyMjQ3MDZaMHwxCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTESMBAGA1UEBxMJU2FudGEgQW5hMQwwCgYDVQQKEwNLS0cxCzAJBgNVBAsTAklUMTEwLwYDVQQDEyhuYS1ra2dmcy5rYXZva2Vycmdyb3VwLmNvbS50b2tlbi5zaWduaW5nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9wJ0KYgZiBKTNk/8XVM/SloSSuLEt6GEcgKKcZpwQ9CxWQW/g51EL+lHNGs1NYiYB4vml8xlLXk1pmBBqgeCuIBhqP+MNigouCGT4jen68erPP/NGgyrYxl4oKqqO+poDKOjojcSQ16xVTSjfkUkKiUJxQGMaS5/KhwdAu00XzLPq0y1EctIPC9hBjp8egwdji4SlcN7Df7E0iBCNPkS9TbSRtgOJHwyloOJE6nrChtvRyXHow1evyWmEEg3FFOvh96YfXosljycGHuxuLxvumq6RqXFWgFH6ibeJqb2eHoT4S/LETslGczpCVaFvCPDbVsRIRMtLvTUNaBzhwvidwIDAQABo4IDMDCCAywwPgYJKwYBBAGCNxUHBDEwLwYnKwYBBAGCNxUIh9qTcYSxgj2G4ZcphsakAIPpxzqBE4Wc/UCCgMJWAgFkAgEXMBMGA1UdJQQMMAoGCCsGAQUFBwMBMAsGA1UdDwQEAwIFoDAbBgkrBgEEAYI3FQoEDjAMMAoGCCsGAQUFBwMBMB0GA1UdDgQWBBSFwM8UdgCJa4Wplg/WaDICCYxbojAfBgNVHSMEGDAWgBQNdA7U+vC6WSD/E6mfmKg/Ghcw1jCCASQGA1UdHwSCARswggEXMIIBE6CCAQ+gggELhoHDbGRhcDovLy9DTj1TeWJyb25FbnRDQSxDTj1vcmctdm1zc2NhZTEsQ049Q0RQLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9c2RzLERDPXN5YnJvbmRlbnRhbCxEQz1jb20/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1dGlvblBvaW50hkNodHRwOi8vb3JnLXZtc3NjYWUxLnNkcy5zeWJyb25kZW50YWwuY29tL0NlcnRFbnJvbGwvU3licm9uRW50Q0EuY3JsMIIBQQYIKwYBBQUHAQEEggEzMIIBLzCBtgYIKwYBBQUHMAKGgalsZGFwOi8vL0NOPVN5YnJvbkVudENBLENOPUFJQSxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPXNkcyxEQz1zeWJyb25kZW50YWwsREM9Y29tP2NBQ2VydGlmaWNhdGU/YmFzZT9vYmplY3RDbGFzcz1jZXJ0aWZpY2F0aW9uQXV0aG9yaXR5MHQGCCsGAQUFBzAChmhodHRwOi8vb3JnLXZtc3NjYWUxLnNkcy5zeWJyb25kZW50YWwuY29tL0NlcnRFbnJvbGwvb3JnLXZtc3NjYWUxLnNkcy5zeWJyb25kZW50YWwuY29tX1N5YnJvbkVudENBKDEpLmNydDANBgkqhkiG9w0BAQsFAAOCAQEAIUf64vx8Wo9/w0uOP7/SpkSqXBfYcVP2JbGP7SCaEAiGfnByuvmuHn8P2z7XHJ82Cj4KJ55fpSyLiD9efq9Q4ENZd0WR4j/fLOqAMuEM5RdCE7VbnSVU2lJK7c5R/2clH6ura9VakOHOzm7647BXRm9Mu/aLQgjGCpUO0M9MA+1/QbybbmiM2ncszXvTKA44vu/b2V8Jy2sjDj4ls/APQosSKFqIxX4d70qrVdMOyPrZOenI9cs+laZ1CcsKa1GBbph942aWE7cxdQxP+ftFxiz85za1k4iJQTdH2jVlyg4Ryte4vcbZeI+q5D4fppkANSeOqq1dwlWMx6fAjGj/FA==',
    ),
  ),
  'saml20.sign.assertion' => true,
);

//DEV & TEST
$metadata['http://na-kkgfsdev.kavokerrgroup.com/adfs/services/trust'] = array (
  'entityid' => 'http://na-kkgfsdev.kavokerrgroup.com/adfs/services/trust',
  'contacts' => 
  array (
    0 => 
    array (
      'contactType' => 'support',
    ),
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://na-kkgfsdev.kavokerrgroup.com/adfs/ls/',
      'index' => 0,
      'isDefault' => true,
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
      'Location' => 'https://na-kkgfsdev.kavokerrgroup.com/adfs/ls/',
      'index' => 1,
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://na-kkgfsdev.kavokerrgroup.com/adfs/ls/',
      'index' => 2,
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://na-kkgfsdev.kavokerrgroup.com/adfs/ls/',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://na-kkgfsdev.kavokerrgroup.com/adfs/ls/',
    ),
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIG2DCCBcCgAwIBAgITHQABzlCKokkSWBX8GQABAAHOUDANBgkqhkiG9w0BAQsFADBeMRMwEQYKCZImiZPyLGQBGRYDY29tMRwwGgYKCZImiZPyLGQBGRYMc3licm9uZGVudGFsMRMwEQYKCZImiZPyLGQBGRYDc2RzMRQwEgYDVQQDEwtTeWJyb25FbnRDQTAeFw0xNjAzMzAxOTM1MDhaFw0yMDAzMjkxOTM1MDhaMIG6MQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExDzANBgNVBAcTBk9yYW5nZTEYMBYGA1UEChMPS2F2byBLZXJyIEdyb3VwMQswCQYDVQQLEwJJVDE3MDUGA1UEAxMubmEta2tnZnNkZXYua2F2b2tlcnJncm91cC5jb20udG9rZW4uZGVjcnlwdGluZzEtMCsGCSqGSIb3DQEJARYeY2hyaXMudG9ibGVyQGthdm9rZXJyZ3JvdXAuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0w6e/c1cRMOvekWCQb5iroNdRvSa0oldx41a37STBrUBl+pjPozCRHYTJVAZ4JonDQ89A2A8l/saMuSj2U3a2yg6fGYY58VXB7Vb8cqV02ZYWRfmNzeywMjj/JoJx3UiT6IeAql/HqbHWlaAapeYuB+N25sNxSDToiCoiJWEltieDGvFztkq6ORjGGqZYM0SjU0hWEVSc6fWzKZ5y0RuRKyCLTHQ2Re04TtUm3JsDldKjcNhYKwGU348QozmJCa793puMVj+G2QLOlJg2UnoWzEmFjmr6t68G6FsWTFB2WpGTlMTf9a83WPz2S7sLdySI0+8w8zRgtTNMB82d4DS9wIDAQABo4IDMDCCAywwCwYDVR0PBAQDAgWgMD4GCSsGAQQBgjcVBwQxMC8GJysGAQQBgjcVCIfak3GEsYI9huGXKYbGpACD6cc6gROGyOZ6g7q4NwIBZAIBCjAdBgNVHQ4EFgQUzCyIDiIbdqD848jTBOiugPyuGM4wHwYDVR0jBBgwFoAUDXQO1Prwulkg/xOpn5ioPxoXMNYwggEkBgNVHR8EggEbMIIBFzCCAROgggEPoIIBC4aBw2xkYXA6Ly8vQ049U3licm9uRW50Q0EsQ049b3JnLXZtc3NjYWUxLENOPUNEUCxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPXNkcyxEQz1zeWJyb25kZW50YWwsREM9Y29tP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3Q/YmFzZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRpb25Qb2ludIZDaHR0cDovL29yZy12bXNzY2FlMS5zZHMuc3licm9uZGVudGFsLmNvbS9DZXJ0RW5yb2xsL1N5YnJvbkVudENBLmNybDCCAUEGCCsGAQUFBwEBBIIBMzCCAS8wgbYGCCsGAQUFBzAChoGpbGRhcDovLy9DTj1TeWJyb25FbnRDQSxDTj1BSUEsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1zZHMsREM9c3licm9uZGVudGFsLERDPWNvbT9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNhdGlvbkF1dGhvcml0eTB0BggrBgEFBQcwAoZoaHR0cDovL29yZy12bXNzY2FlMS5zZHMuc3licm9uZGVudGFsLmNvbS9DZXJ0RW5yb2xsL29yZy12bXNzY2FlMS5zZHMuc3licm9uZGVudGFsLmNvbV9TeWJyb25FbnRDQSgxKS5jcnQwEwYDVR0lBAwwCgYIKwYBBQUHAwEwGwYJKwYBBAGCNxUKBA4wDDAKBggrBgEFBQcDATANBgkqhkiG9w0BAQsFAAOCAQEAcan/q8t1IWqblEd4WT73dF51EkX4ZTw0Gx6Rcyyo99EESsZANXplly9WuH2SzJL+LT1ipYt0tdrA7jTPnLXtaNITOrryLN/VWmL8rpu0/aD7qbPaahOeknnSUk5xnCFXLUWgF5GeW9pd5ZUiBAb9WkyhYZ6mOaq8eMVnul/bpoFijezrkDh5UIH2X43uTc3opdMlJWW9dAgfjbq2m3uxWT3K18KAb2tdgFw9l3MgoEFyr+jVcZvUNLV2htbz9rvPdhiFo2r/9GACaaJWoY1h532Tco+gaOc9hyfh3FzCYJkwAzJUZm/8swUAXbbvJeLPuet7+5RuNxInLk2AFUTrGA==',
    ),
    1 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIG1TCCBb2gAwIBAgITHQABzhr+f9VAaFKI3QABAAHOGjANBgkqhkiG9w0BAQsFADBeMRMwEQYKCZImiZPyLGQBGRYDY29tMRwwGgYKCZImiZPyLGQBGRYMc3licm9uZGVudGFsMRMwEQYKCZImiZPyLGQBGRYDc2RzMRQwEgYDVQQDEwtTeWJyb25FbnRDQTAeFw0xNjAzMjkyMzEyMzRaFw0yMDAzMjgyMzEyMzRaMIG3MQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExDzANBgNVBAcTBk9yYW5nZTEYMBYGA1UEChMPS2F2byBLZXJyIEdyb3VwMQswCQYDVQQLEwJJVDE0MDIGA1UEAxMrbmEta2tnZnNkZXYua2F2b2tlcnJncm91cC5jb20udG9rZW4uc2lnbmluZzEtMCsGCSqGSIb3DQEJARYeY2hyaXMudG9ibGVyQGthdm9rZXJyZ3JvdXAuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1i9xj7FQd7of7IAPjEP0X4TAK1HQG1xbil+I6uIRzM4agcm3+0wTCCCQJ+L1hpnEE/2TEbLNPjIRNq3+6ltGPYe1tXf1k3p/GwRFvMqBujJOnJhI27EqBY0wndTu0tERYn61J+IHSnz1j0W0N5y5L0DI+cRyez3XUjaMCd3VRZPo7TxNq8ZeYEE4Uh85P1MiQnIuH4iJE+8xCEOjdSRcqHHDnw1Q2C6/Sz2cGU2i16QYjJiPaZnBKg7XBJj3DOYmVUAdfkaMBwz7PA0FH715rwPg/uGrk1XzKbOFylqchcXSeVjwTi8by7ys6KY5luCt0XI+8OQszT6mgXe0EpAVqQIDAQABo4IDMDCCAywwCwYDVR0PBAQDAgWgMD4GCSsGAQQBgjcVBwQxMC8GJysGAQQBgjcVCIfak3GEsYI9huGXKYbGpACD6cc6gROGyOZ6g7q4NwIBZAIBCTAdBgNVHQ4EFgQUz6frFTq7jnSOTJxNZreLMK8NN1MwHwYDVR0jBBgwFoAUDXQO1Prwulkg/xOpn5ioPxoXMNYwggEkBgNVHR8EggEbMIIBFzCCAROgggEPoIIBC4aBw2xkYXA6Ly8vQ049U3licm9uRW50Q0EsQ049b3JnLXZtc3NjYWUxLENOPUNEUCxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPXNkcyxEQz1zeWJyb25kZW50YWwsREM9Y29tP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3Q/YmFzZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRpb25Qb2ludIZDaHR0cDovL29yZy12bXNzY2FlMS5zZHMuc3licm9uZGVudGFsLmNvbS9DZXJ0RW5yb2xsL1N5YnJvbkVudENBLmNybDCCAUEGCCsGAQUFBwEBBIIBMzCCAS8wgbYGCCsGAQUFBzAChoGpbGRhcDovLy9DTj1TeWJyb25FbnRDQSxDTj1BSUEsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1zZHMsREM9c3licm9uZGVudGFsLERDPWNvbT9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNhdGlvbkF1dGhvcml0eTB0BggrBgEFBQcwAoZoaHR0cDovL29yZy12bXNzY2FlMS5zZHMuc3licm9uZGVudGFsLmNvbS9DZXJ0RW5yb2xsL29yZy12bXNzY2FlMS5zZHMuc3licm9uZGVudGFsLmNvbV9TeWJyb25FbnRDQSgxKS5jcnQwEwYDVR0lBAwwCgYIKwYBBQUHAwEwGwYJKwYBBAGCNxUKBA4wDDAKBggrBgEFBQcDATANBgkqhkiG9w0BAQsFAAOCAQEAdyqLMUKiYu/kLqb2i8LD1pQvQT6ix6MUVhz4c0V/xs68QSkh0H4WGP9YXrdZLj+xNJxxmgKF/i3+EDClho2wmZ7Y+Bxa3p7YPXek3jTeXyL48mOJse37MvpTNSGRwuy9RB3v7xQFqM3tIDXTIy+cIYGdRdOeGYIUWNdJhOZoqKQqJoldXI8/y2giFW38O6USRho3EOQ60Wa5Im3t+uP9j7yG+imyjKVoWaUqTEcbNEepQlWMUG25Om+XR6E6sjq0+pQ5oAVgDO/Oc29gzRZv5JIjuodUBjV4yc2hBtui2Zf1wIIDZBc8bFl5NufusGkpZQ7mTXTKo8NmoAavoIFfWg==',
    ),
  ),
  'saml20.sign.assertion' => true,
);
