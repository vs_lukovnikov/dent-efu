jQuery(document).ready(function ($) {
  // var isIE = /*@cc_on!@*/false || !!document.documentMode
  // var isEdge = !isIE && !!window.StyleMedia
  // if (isIE || isEdge) {
  //   $('body').addClass('ie-br')
  // }  else {
  //   $('body').addClass('not-ie-br')
  // }
  // if ($('body').hasClass('ie-br')) {
  //   if (window.matchMedia("(min-width: 993px)").matches) {
  //     $('.right-side').scrollToFixed({
  //       marginTop: 50
  //     })
  //   }
  // }

  $('#choose-language').click(function (e) {
        e.preventDefault();
        $('#language-region').slideToggle();
    });

    $('li.dropdown .caret').click(function (e) {
        var $li = $(this).closest('li');
        $li.find('> ul.dropdown-menu').slideToggle();
    });

    $('button.navbar-toggle').click(function () {
        $('#language-region').slideUp();
    });

    $('.view-downloads-center').parent().on('click', '.view-downloads-center a.request-printed-version', function (e) {
        e.preventDefault();
        var link = $(this);
        var widen_asset = link.closest('.download-item').find('.content.views-fieldset .views-field-nid .field-content');
        $('#request-printed-version .webform-component--widen-asset input[type=hidden]').val(widen_asset.html());
        $('#request-printed-version').modal('show');
    });

    $('#request-printed-version').on('hidden.bs.modal', function () {
        var confirmationMessage = $('#request-printed-version .webform-confirmation');
        if (confirmationMessage.length){
            $('#go-back-to-form').mousedown();
        }
    });

    var right_side_size = function() {
        if (window.matchMedia("(max-width: 992px)").matches) {
            $('.content-side').css('height', 'auto').css('width', 'auto');
            $('aside.right-side .side-block').css('height', 'auto').css('width', 'auto');
            $('footer aside').css('width', 'auto');
            return;
        }
        // height
        var block_count = $('aside.right-side').length;
        var max_height = Math.floor($(window).outerHeight() - 50 - $('footer').outerHeight()); // 50 = header height
        var block_height = Math.floor(max_height / block_count);
        $('aside.right-side').each(function() {

            var block_min_height = parseInt($(this).attr('data-min-height')) || 0;
            var block_min_width = parseInt($(this).attr('data-min-width')) || 0;
            var block_max_width = parseInt($(this).attr('data-max-width')) || 9999;

            block_width = block_height; // make square

            if (block_min_height > block_height) { // enforce min height
                block_height = block_min_height;
            }

            if (block_min_width > block_width) { // enforce min width
                block_width = block_min_width;
            }

            if (block_max_width < block_width) { // enforce max width
                block_width = block_max_width; // don't go wider than max width
            }

            $(this).css('height', block_height + 'px').css('width', block_width + 'px');

        });
        // width
        var max_width = $('.main-container > .container').width() - $('aside.right-side').width();
        $('.content-side').css('width', max_width + 'px');//.css('height', max_height + 'px');
        // if ($('.content-side').outerHeight() < max_height) {
        // $('.content-side').css('min-height', max_height + 'px');
        // }
        // footer
        // $('footer aside').css('width', $('aside.right-side').width() + 'px');
        var offsetRight = $('.main-container > .container').offset().left;
        $('aside.right-side').css('right', offsetRight + 15);
    };

    if ($('aside.right-side').length > 0) {
        right_side_size();
    }

    $(window).resize(function () {
        right_side_size();
        // if ($('body').hasClass('ie-br')) {
        //   if (window.matchMedia("(min-width: 993px)").matches) {
        //     $('.right-side').scrollToFixed({
        //       marginTop: 50
        //     })
        //   }
        // }
    });

    $('.view-downloads-center').parent().on('click', '.see-more-link', function() {
        $(this).parent().find('.see-more').css('display', 'inline');
        $(this).remove();
        return false;
    });

    // Drupal.ajax.prototype.commands.viewsScrollTop = function (ajax, response, status) {
    //     // Scroll to the top of the view. This will allow users
    //     // to browse newly loaded content after e.g. clicking a pager
    //     // link.
    //     var offset = $(response.selector).offset();
    //     // We can't guarantee that the scrollable object should be
    //     // the body, as the view could be embedded in something
    //     // more complex such as a modal popup. Recurse up the DOM
    //     // and scroll the first element that has a non-zero top.
    //     var scrollTarget = response.selector;
    //     while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
    //         scrollTarget = $(scrollTarget).parent();
    //     }
    //     // Only scroll upward
    //     if (offset.top - 10 < $(scrollTarget).scrollTop()) {
    //         if ($('body').hasClass('logged-in')){
    //             $(scrollTarget).animate({scrollTop: (offset.top - 120)}, 500);
    //         } else {
    //             $(scrollTarget).animate({scrollTop: (offset.top - 50)}, 500);
    //         }
    //     }
    // };

  // header help
  // $('#header-help').click(function() {
  //   if ($('#header-help .help-text').is(':visible')) {
  //     $('#header-help .help-text').addClass('out');
  //     $('#header-help .help-phone').removeClass('out');
  //     $('#header-help .help-container').addClass('open',400,"easeOutBounce");
  //     $('#header-help .help-phone i').addClass('shake');
  //     setTimeout(function() {
  //       $('#header-help .help-phone i').removeClass('shake')
  //       $('#header-help .help-text').removeClass('out');
  //       $('#header-help .help-phone').addClass('out');
  //       $('#header-help .help-container').removeClass('open');
  //     }, 4000);
  //   }
  // });

  $('#edit-field-resource-product-tid, #edit-field-resource-part-number-tid').select2({
    width: '100%',
    }
  );

  var selectWidth = jQuery('.form-item').width();

  jQuery('body').on('change', 'span.select2-container--open', function(e) {
    jQuery(this).width(selectWidth);
  });

  $(function() {
    $('#edit-combine').keypress(function() {
      $('#edit-field-resource-part-number-tid').val('All');
      $('#edit-field-resource-product-tid').val('All');
      $('#edit-all-languages').prop('checked', false);
    });

    $('#edit-field-resource-product-tid').change(function() {
      $('#edit-field-resource-part-number-tid').val('All');
      $('#edit-combine').val('');
      $('#edit-all-languages').prop('checked', false);
      this.form.submit();
    });

    $('#edit-field-resource-part-number-tid').change(function() {
      $('#edit-field-resource-product-tid').val('All');
      $('#edit-combine').val('');
      $('#edit-all-languages').prop('checked', false);
      this.form.submit();
    });

    $(document).on('change', '#edit-all-languages', function() {
        this.form.submit();
    });
  });

  $('.form-item-combine').append('<button id="search-input-btn"></button>')

  $('.view-header-tabs-view .views-field-title .field-content span').on('click', function () {

    if (!$(this).closest('.views-row').hasClass('active-tab')) {
      $('.view-header-tabs-view .views-row').removeClass('active-tab')
      $(this).closest('.views-row').addClass('active-tab')
    }

    var trigger = $(this);
    $(".collapse.in").each(function(){
      if( trigger.attr("data-target") != ("#"+$(this).attr("id")) ){
        $(this).collapse('hide');
      } // condition returns false on iteration on div to be opened
    });
  });

  $('.view-header-tabs-view .view-content').append('<div class="phone-help"></div>');
  $(".header-tabs .header-help").detach().appendTo(".view-header-tabs-view .view-content .phone-help");

  $('.view-header-tabs-view .view-content').append('<button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>')

  $('.view-header-tabs-view .views-row:first-child').addClass('active-tab');
  $('.view-header-tabs-view .views-row:first-child .views-fieldset').addClass('in');

  $('.view-header-tabs-view .view-content .close').on('click', function () {
    $('.header-tabs').addClass('hide-overlay');
    $('body .overlay').addClass('hide-overlay');
    $('#header-help').removeClass('help-active')
    $('.view-header-tabs-view .views-fieldset').removeClass('in')
    $('.view-header-tabs-view .views-row:first-child .views-fieldset').collapse('show');
    $('.view-header-tabs-view .views-row').removeClass('active-tab');
    $('.view-header-tabs-view .views-row:first-child').addClass('active-tab');
  });

  $('#header-help').on('click', function () {
    $(this).addClass('help-active');
    if ($('.header-tabs').hasClass('hide-overlay')) {
      $('.header-tabs').removeClass('hide-overlay');
      $('body .overlay').removeClass('hide-overlay');
    } else {
      $('#header-help').addClass('help-active');
      $('.header-tabs').addClass('hide-overlay');
      $('body .overlay').addClass('hide-overlay');
      $(this).removeClass('help-active');
    }

    if (!$(this).hasClass('help-active')) {
      $('.view-header-tabs-view .views-fieldset').removeClass('in')
      $('.view-header-tabs-view .views-row:first-child .views-fieldset').collapse('show');
      $('.view-header-tabs-view .views-row').removeClass('active-tab');
      $('.view-header-tabs-view .views-row:first-child').addClass('active-tab');
    }
  });

  $('.view-header-tabs-view .views-field-title .field-content span').on('click',function(e) {
    if($(this).parents('.views-row').children('.views-fieldset').hasClass('in')){
      e.stopPropagation();
    }
  });

  //Add padding to instructions blocks depending on tabs height
  var $window = $(window);
  var language = $('#choose-language span').text().trim();

  function checkWidth() {
    var windowsize = $window.width();
    if (windowsize < 992) {
      var trimmedLang = language.substring(0,2);
      $('#choose-language span').text(trimmedLang);
    } else {
      $('#choose-language span').text(language);
    }

    $('.view-header-tabs-view .views-fieldset').css('top', $(".header-tabs").height());
  }
  // Execute on load
  checkWidth();
  // Bind event listener
  $(window).resize(checkWidth);
});

jQuery(document).mouseup(function (e) {
    if (window.matchMedia("(min-width: 1024px)").matches) {
        var container = jQuery("#language-region");
        if (container.has(e.target).length === 0) {
            container.hide();
        }
    }
});
