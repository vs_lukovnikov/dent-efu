<?php

/**
 * Implements view more/less functionality
 *
 * @param $label string
 * @param $vars
 */
function eifu_download_center_views_view_more($label, $vars) {
  if (!empty($vars)) {
    $values = $vars[LANGUAGE_NONE];

    if (is_array($values)) {
      ?>
        <div class="views-field views-field-php">
                <span class="field-content">
                    <div>
                        <div class="read-more-wr">
                            <?php
                            if (!empty($label)) {
                              print '<div class="label-inline">' . ucfirst($label) . ':</div>';
                            }
                            $i = 1;
                            foreach ($values as $value) {
                              print "<span class='read-more-item'>{$value['value']}";
                              if (count($values) !== $i) {
                                print ", ";
                              }
                              print "</span>";
                              $i++;
                            }
                            ?>
                        </div>
                    </div>
                </span>
        </div>
      <?php
    }
  }
}

/**
 * Custom node template for front page
 *
 * @param $vars
 */
function eifu_download_center_preprocess_node(&$vars) {
  if ($vars["is_front"]) {
    $vars["theme_hook_suggestions"][] = "node__page__front";
  }
}

/**
 * Returns HTML for a menu link and submenu.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_menu_link()
 *
 * @ingroup theme_functions
 */
function eifu_download_center_menu_link__menu_language_menu(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  if ($element['#original_link']['depth'] == 1) {
    $output = '<h4>' . $element['#title'] . '</h4>';
    if (array_search('active', $element['#attributes']['class'])) {
      unset($element['#attributes']['class'][array_search('active', $element['#attributes']['class'])]);
    }
  }
  else {

    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  }

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Returns HTML for a menu link and submenu.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_menu_link()
 *
 * @ingroup theme_functions
 */
function eifu_download_center_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  $options = !empty($element['#localized_options']) ? $element['#localized_options'] : [];

  // Check plain title if "html" is not set, otherwise, filter for XSS attacks.
  $title = empty($options['html']) ? check_plain($element['#title']) : filter_xss_admin($element['#title']);

  // Ensure "html" is now enabled so l() doesn't double encode. This is now
  // safe to do since both check_plain() and filter_xss_admin() encode HTML
  // entities. See: https://www.drupal.org/node/2854978
  $options['html'] = TRUE;

  $href = $element['#href'];
  $attributes = !empty($element['#attributes']) ? $element['#attributes'] : [];

  if ($element['#below']) {
    // Prevent dropdown functions from being added to management menu so it
    // does not affect the navbar module.
    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    }
    elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
      // Add our own wrapper.
      unset($element['#below']['#theme_wrappers']);
      $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';

      // Generate as standard dropdown.
      $attributes['class'][] = 'dropdown';

      // Set dropdown trigger element to # to prevent inadvertant page loading
      // when a submenu link is clicked.
      $options['attributes']['data-target'] = '#';
      //            $options['attributes']['class'][] = 'dropdown-toggle';
      //            $options['attributes']['data-toggle'] = 'dropdown';

      return '<li' . drupal_attributes($attributes) . '>' . l($title, $href, $options) . ' <span class="caret"></span>' . $sub_menu . "</li>\n";
    }
  }

  return '<li' . drupal_attributes($attributes) . '>' . l($title, $href, $options) . $sub_menu . "</li>\n";
}

/**
 * Returns HTML for a form element label and required marker.
 *
 * Form element labels include the #title and a #required marker. The label is
 * associated with the element itself by the element #id. Labels may appear
 * before or after elements, depending on theme_form_element() and
 * #title_display.
 *
 * This function will not be called for elements with no labels, depending on
 * #title_display. For elements that have an empty #title and are not required,
 * this function will output no label (''). For required elements that have an
 * empty #title, this will output the required marker alone within the label.
 * The label will use the #id to associate the marker with the field that is
 * required. That is especially important for screenreader users to know
 * which field is required.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #required, #title, #id, #value, #description.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_form_element_label()
 *
 * @ingroup theme_functions
 */
function eifu_download_center_form_element_label(&$variables) {
  $element = $variables['element'];

  // Extract variables.
  $output = '';

  $title = !empty($element['#title']) ? filter_xss_admin($element['#title']) : '';

  // Only show the required marker if there is an actual title to display.
  if ($title && $required = !empty($element['#required']) ? theme('form_required_marker', ['element' => $element]) : '') {
    $title .= ' ' . $required;
  }

  $display = isset($element['#title_display']) ? $element['#title_display'] : 'before';
  $type = !empty($element['#type']) ? $element['#type'] : FALSE;
  $checkbox = $type && $type === 'checkbox';
  $radio = $type && $type === 'radio';

  // Immediately return if the element is not a checkbox or radio and there is
  // no label to be rendered.
  if (!$checkbox && !$radio && ($display === 'none' || !$title)) {
    return '';
  }

  // Retrieve the label attributes array.
  $attributes = &_bootstrap_get_attributes($element, 'label_attributes');

  // Add Bootstrap label class.
  $attributes['class'][] = 'control-label';

  // Add the necessary 'for' attribute if the element ID exists.
  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }
  $rendered_element = '';
  // Checkboxes and radios must construct the label differently.
  if ($checkbox || $radio) {
    if ($display === 'before') {
      $output .= $title;
    }
    elseif ($display === 'none' || $display === 'invisible') {
      $output .= '<span class="element-invisible">' . $title . '</span>';
    }
    // Inject the rendered checkbox or radio element inside the label.
    if (!empty($element['#children'])) {
      $rendered_element = $element['#children'];
    }
    if ($display === 'after') {
      $output .= $title;
    }
  }
  // Otherwise, just render the title as the label.
  else {
    // Show label only to screen readers to avoid disruption in visual flows.
    if ($display === 'invisible') {
      $attributes['class'][] = 'element-invisible';
    }
    $output .= $title;
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return $rendered_element . ' <label' . drupal_attributes($attributes) . '>' . $output . "</label>\n";
}

/**
 * Implements hook_pre_render().
 */
function eifu_download_center_pre_render($element) {
  // add * to placeholder for required fields
  if (isset($element['#webform_placeholder']) && $element['#required']) {
    $element['#attributes']['placeholder'] .= ' *';
  }

  if (isset($element['#webform_component']) && $element['#type'] == 'select' && $element['#required']) {
    $element['#options'][''] .= ' *';
  }

  // Return the modified element.
  return $element;
}

function eifu_download_center_links(&$variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = [
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        ];
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(['class' => $heading['class']]);
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = [$key];

      // Add first, last and active classes to the list of links to help out
      // themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
        && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }
      $output .= '<li' . drupal_attributes(['class' => $class]) . '><div dir="rtl">';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for
        // adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</div></li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Create view more/less html
 *
 * @param $classes
 * @param string $label
 * @param array $items
 *
 * @return string
 */
function eifu_download_center_field_view_more($classes, $items, $label = '') {
  if (!empty($items)) {
    $classes .= ' field-more closed';
    $content = '';
    $output = '';
    $items_count = count($items);

    if (!empty($label)) {
      $content .= '<div class="field-label">' . ucfirst($label) . ':</div>';
    }

    if ($items_count > 1) {
      $classes .= ' field-more closed multiple';

      if ($items_count > 3) {
        $teaser_part = implode(", ", array_slice($items, 0, 2));
        $other_part = implode(", ", array_slice($items, 2));

        $content .= "<div class='field-more-teaser'>$teaser_part</div>";
        $content .= eifu_download_center_field_view_more_buttons();
        $content .= "<div class='field-more-other'>$other_part</div>";
      }
      else {
        $content .= implode(", ", $items);
      }

      return "<div class='{$classes}'><div class='field-more-content-multiple'>{$content}</div></div>";
    }
    else {
      $content = $items[0];

      if (strlen($content) > 226) {
        $output = eifu_download_center_field_view_more_buttons();
      }

      return "<div class='{$classes}'><div class='field-more-content'>{$content}</div>{$output}</div>";
    }
  }
}

/**
 * View more buttons html
 *
 * @return string
 */
function eifu_download_center_field_view_more_buttons() {
  $more = t('View More');
  $less = t('View Less');

  $output = "<div class='view-more-wr'>";
  $output .= "<span class='view-more more'>{$more}</span>";
  $output .= "<span class='view-more less'>{$less}</span>";
  $output .= "</div>";

  return $output;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function eifu_download_center_preprocess_page(&$variables) {
  global $language;

      $variables['need_help'] = [
        'title' => variable_get('header_help_title_' . $language->language, 'Need Help'),
        'text' => variable_get('header_help_text_' . $language->language, 'Call 800.537.7123'),
      ];
}

/**
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function  eifu_download_center_form_alter( &$form, &$form_state,$form_id ){
  global $language;
  if($form_id === 'webform_client_form_640' && $language->language == 'ja') {
    $form['submitted']['last_name']['#weight'] = '26';
    $form['submitted']['first_name']['#weight'] = '27';
    $form['submitted']['country']['#weight'] = '30';
    $form['submitted']['postal_code']['#weight'] = '31';
    $form['submitted']['state_or_province']['#weight'] = '32';
    $form['submitted']['city']['#weight'] = '33';
    $form['submitted']['practice_address']['#weight'] = '34';

    uasort($form['submitted'], 'eifu_download_center_form_submit_sort');
  }
  if (isset($form['submitted']) && is_array($form['submitted'])) {
    foreach ($form['submitted'] as $field => &$data) {
      if (is_array($data)) {
        $data['#attributes']['title'] = $data['#title'];
      }
    }
  }
}

function eifu_download_center_form_submit_sort($a, $b) {
  return $a['#weight'] - $b['#weight'];
}
