
<div class="row">
  <div class="col-lg-4 col-md-5 col-sm-6 header-line">
    <h1 class="title">Login</h1>
    <div class="line"><div class="line-inner"></div></div>     
  </div>
</div>
<div class="row">
  <div class="col-md-6">  
    <?php
    	print drupal_render($form['name']);
    	print drupal_render($form['pass']);
    ?>
    
    <?php
      // render login button
    	print drupal_render($form['form_build_id']);
    	print drupal_render($form['form_id']);
    	print drupal_render($form['actions']);
    ?>
    
    <div class="user-login-links">
      <span class="password-link"><a href="/user/password">Request new password</a></span>
    </div>
  </div>
  <div class="col-md-6 sso-login">
    <h3>Kavo Kerr Employee?</h3>
    <p>Click "Go" to connect with your corporate single-sign-on (SSO)</p>
    <div class="sso-login-submit">
      <a class="btn btn-primary" href="/saml_login">Go</a>
    </div>
  </div>    
</div>