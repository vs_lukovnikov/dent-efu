<?php

/**
 * @file
 * Customize the e-mails sent by Webform after successful submission.
 *
 * This file may be renamed "webform-mail-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-mail.tpl.php" to affect all webform e-mails on your site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $submission: The webform submission.
 * - $email: The entire e-mail configuration settings.
 * - $user: The current user submitting the form. Always the Anonymous user
 *   (uid 0) for confidential submissions.
 * - $ip_address: The IP address of the user submitting the form or '(unknown)'
 *   for confidential submissions.
 *
 * The $email['email'] variable can be used to send different e-mails to different users
 * when using the "default" e-mail template.
 */
?>

<?php
    // set EN language for email text
    global $language;
    $all_languages = language_list();
    $current_language = $language;
    $new_language = 'en';
    $language = $all_languages[$new_language];
    $submission->language = $all_languages[$new_language];

    // email text
    $message = '<p>Submitted on [submission:date:long_without_time]</p>';
    if ($user->uid) {
      $message .= '<p>Submitted by user: [submission:user]</p>';
    } else {
      $message .= '<p>Submitted by anonymous user: [submission:ip-address]</p>';
    }

    $message .= '<p>Submitted values are:</p>';
    $message .= '[submission:values]';
    $message .= '<p>The results of this submission may be viewed at:</p>';
    $message .= '<p>[submission:url]</p>';
    $message = webform_replace_tokens($message, $node, $submission, $email, (boolean)$email['html']);
    print $message;

    // Revert current site language
    $language = $current_language;
?>
