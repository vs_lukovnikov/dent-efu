<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php global $language; ?>
<?php $date = date_create($row->field_field_resource_release_date[0]['raw']['value']); ?>
<?php if($language->language == 'ja'): ?>
<?php $custom_date = date_format($date, 'Y.m.d');
  $output = $row->field_field_resource_release_date[0]['raw']['value'] = $custom_date; ?>
  <?php $output = render($row->field_field_resource_release_date[0]['raw']['value']); ?>
<?php elseif($language->language == 'ar'): ?>
<?php $custom_date = date_format($date, 'Y/m/d');
  $output = $row->field_field_resource_release_date[0]['raw']['value'] = $custom_date; ?>
  <?php $output = render($row->field_field_resource_release_date[0]['raw']['value']); ?>
<?php elseif($language->language == 'es'): ?>
<?php $custom_date = date_format($date, 'd.m.Y');
  $output = $row->field_field_resource_release_date[0]['raw']['value'] = $custom_date; ?>
  <?php $output = render($row->field_field_resource_release_date[0]['raw']['value']); ?>
<?php endif; ?>
<?php print $output; ?>
