<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>

<?php
  global $language;
  $output = '';
  $counter = 1;
  $max_items = sizeof($row->field_field_resource_language);
  foreach ($row->field_field_resource_language as $item) {
    if ($max_items == 1) {
      $output .= t($item['rendered']['#markup'], [], ['context' => 'field_widen_languages:' . $item['rendered']['#markup']]);
    } elseif ($max_items == 2) {
      if ($counter == 1) {
        $output .= t($item['rendered']['#markup'], [], ['context' => 'field_widen_languages:' . $item['rendered']['#markup']]);
      }
      else {
        if($language->language === 'ja') {
          $output .= '、' . t($item['rendered']['#markup'], [], ['context' => 'field_widen_languages:' . $item['rendered']['#markup']]);
        } else {
          $output .= ', ' . t($item['rendered']['#markup'], [], ['context' => 'field_widen_languages:' . $item['rendered']['#markup']]);
        }
      }
    } elseif ($max_items > 2) {
      if ($counter == 1) {
        $output .= t($item['rendered']['#markup'], [], ['context' => 'field_widen_languages:' . $item['rendered']['#markup']]) . '<a href="#" class="see-more-link"> ' . t('View More') . '</a><span class="see-more">';
      }
      else {
        if($language->language === 'ja'){
          $output .= '、' . t($item['rendered']['#markup'], [], ['context' => 'field_widen_languages:' . $item['rendered']['#markup']]);
        } else {
          $output .= ', ' . t($item['rendered']['#markup'], [], ['context' => 'field_widen_languages:' . $item['rendered']['#markup']]);
        }
      }
    }
    $counter++;
  }

  if ($max_items > 2 && $counter >= 2) {
    $output .= '</span>'; // close see-more span
  }
?>

<?php print $output; ?>
