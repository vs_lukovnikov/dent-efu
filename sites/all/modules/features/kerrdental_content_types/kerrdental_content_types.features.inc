<?php
/**
 * @file
 * kerrdental_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kerrdental_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function kerrdental_content_types_node_info() {
  $items = array(
    'resource_asset' => array(
      'name' => t('Resource Asset'),
      'base' => 'node_content',
      'description' => t('Resource Center Asset'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
