<?php
/**
 * @file
 * kerrdental_content_types.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function kerrdental_content_types_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_internal|node|resource_asset|form';
  $field_group->group_name = 'group_internal';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_asset';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Internal',
    'weight' => '14',
    'children' => array(
      0 => 'field_resource_content',
      1 => 'field_resource_uuid',
      2 => 'field_resource_widen_expires',
      3 => 'field_resource_widen_updated',
      4 => 'field_resource_popularity',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-internal field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_internal|node|resource_asset|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Internal');

  return $field_groups;
}
