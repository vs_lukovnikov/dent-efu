// Register the plugin within the editor.
CKEDITOR.plugins.add( 'widendam', {

    // The plugin initialization logic goes inside this method.
    init: function( editor ) {
        var config = editor.config;
        var widendamConfig=Drupal.settings["widendam"];

        // Register the toolbar buttons.
        editor.ui.addButton( 'widendam',
            {
                label : Drupal.t('Widen DAM'),
                icon : this.path + 'icons/widendam_logo.png',
                command : 'widendamWindow'
            });


        //opening popup window
        editor.addCommand( 'widendamWindow',
            {
            exec : function () {

                var overlay;
                var container;
                var iframe;

                if(jQuery('#widendam-overlay').length)
                {
                    // overlay and iframe elements already exist, simply show them
                    overlay = jQuery('#widendam-overlay');
                    container = jQuery('#widendam-container');
                    iframe = jQuery('#widendam-iframe');

                    overlay.show();
                    container.show();
                    iframe.show();
                }

                else
                {
                    // iframe and overlay divs must be created
                    var body = document.getElementsByTagName("body")[0];

                    overlay = document.createElement("div");
                    overlay.setAttribute("id", "widendam-overlay");
                    overlay.setAttribute("style",
                        "position: fixed;"+
                            "top: 0px;"+
                            "right: 0px;"+
                            "bottom: 0px;"+
                            "left: 0px;"+
                            "z-index: 10000;"+
                            "background-color: rgb(0,0,0);"+
                            "background-color: rgba(0,0,0,0.5);"
                    );

                    var closeIcon = document.createElement("span");

                    container = document.createElement("div");
                    container.setAttribute("id", "widendam-container");
                    container.setAttribute("style",
                        "position: fixed;"+
                            "top: 50px;"+
                            "bottom: 50px;"+
                            "left: 50px;"+
                            "right: 50px;"+
                            "background-color: rgb(255,255,255);"
                    );

                    iframe = document.createElement("iframe");
                    //iframe setup:
                    iframe.src = Drupal.settings.fullPath + 'admin/content/widen/embed';
                    iframe.style.position = "absolute";
                    iframe.style.top = "0px";
                    iframe.style.left = "0px";
                    iframe.style.border = "0px";
                    iframe.style.width = "100%";
                    iframe.style.height = "100%";
                    iframe.id = "widendam-iframe";

                    container.appendChild(iframe);
                    overlay.appendChild(container);
                    body.appendChild(overlay);

                    jQuery('#widendam-overlay').bind('click', function()
                    {
                        var overlay = jQuery('#widendam-overlay' );
                        var container = jQuery('#widendam-container');
                        var iframe = jQuery('#widendam-iframe');

                        iframe.hide();
                        container.hide();
                        overlay.hide();
                    });
                }


                window.widendam_embedCallback = function(uuid, msg)
                {
                    
                  if (msg.indexOf('href') > 1) {
                    
                    var code = removeSizeTags(msg);
                    editor.insertHtml(code);
                    overlay = jQuery('#widendam-overlay');
                    container = jQuery('#widendam-container');
                    iframe = jQuery('#widendam-iframe');

                    iframe.hide();
                    container.hide();
                    overlay.hide();
                    
                  } else {
                    
                    var src_end = msg.split('src="');
                    var src_end2 = src_end[1].split('"');
                    var url = src_end2[0];
                    var node_path = window.location.pathname.replace('/node/', '').split('/');
                    jQuery.ajax({
                      url: Drupal.settings.basePath + 'ajax/kerrdental-widen/save-asset',
                      type: 'post',
                      data: 'nodeid=' + node_path[0] + '&uuid=' + encodeURIComponent(uuid) + '&url=' + encodeURIComponent(url),
                      success: function(r){
                        if (r.status == 'success') {
                          editor.insertHtml(r.fullsize);
                          overlay = jQuery('#widendam-overlay');
                          container = jQuery('#widendam-container');
                          iframe = jQuery('#widendam-iframe');
      
                          iframe.hide();
                          container.hide();
                          overlay.hide();
                          return;
                        } else if (r.status == 'error') {
                          alert(r.message);
                        }
                      }
                    });
                    
                  } 
                   
                }

                // Remove invalid img size attributes
                removeSizeTags = function(code)
                {
                    var clean = code.replace('height="0"', '');
                    clean = clean.replace('width="0"', '');
                    return clean;
                }

            }

        });

    }
});
