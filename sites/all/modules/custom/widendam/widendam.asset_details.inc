<?php

function widendam_asset_details($uuid)
{
    drupal_add_js(drupal_get_path('module', 'widendam') . '/js/widendam_asset_details.js');
    drupal_add_css(drupal_get_path('module', 'widendam') . '/css/widendam_asset_details.css');

    $asset = widendam_api_search_by_uuid($uuid);
    $codes = widendam_api_get_embed_codes($uuid);

    $build = array();
    $content = array();

    $content['details'] = drupal_get_form('widendam_asset_details_form', $asset, $codes);
    $build['content'] = $content;

    return $build;
}

function widendam_asset_details_form($form, &$form_state, $asset, $codes)
{
    $form['filename'] = array(
        '#type' => 'markup',
        '#markup' => '<h1>' . $asset->filename . '</h1>',
    );

    $form['details'] = array(
        '#type' => 'fieldset',
        '#attributes' => array(
            'class' => array('widen_asset_details'),
        ),
    );

    $preview_markup = '<div class="widen_preview_image"><img src="' . $asset->previews->preview300 . '"/></div>';
    $metadata_markup = '<div class="widen_metadata">';

    if (isset($asset) && isset($asset->metadata))
    {
        foreach($asset->metadata as $field)
        {
//            dpm($field->field . ': ' . $field->value);
            if (isset($field->value))
            {
                $metadata_markup .= '<b>' . $field->field . '</b><br/>' . $field->value . '<br/><br/>';
            }
        }
    }

    $metadata_markup .= '</div>';

    $form['details']['preview'] = array(
        '#type' => 'markup',
        '#markup' => $preview_markup . $metadata_markup,
    );

    if (isset($codes) && isset($codes->embedCodes))
    {
        $embed_codes = array();
        foreach($codes->embedCodes as $key => $code)
        {
            $embed_codes[$code] = $key;
        }

        if (isset($embed_codes) && !empty($embed_codes))
        {
            $form['details']['codes'] = array(
                '#type' => 'select',
                '#title' => t('Embed Codes'),
                '#id' => 'widen_embed_select',
                '#options' => $embed_codes,
            );

            $form['details']['code'] = array(
                '#type' => 'textarea',
                '#id' => 'widen_embed_text_area',
                '#default_value' => key($embed_codes),   // First key
            );
        }
    }

    return $form;

}