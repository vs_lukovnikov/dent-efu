<?php

define('WIDEN_CLIENT_REGISTRATION', '167d00b8e36e37dc47d4fb9876192c003f6d8e0a.app.widen.com');


function widendam_api_get_login_link() {
  global $base_root;

  $returnLink = $base_root . url("user/widen/auth") . "&response_type=code";
  $collectiveDomain = widendam_variable_get('collectiveDomain');
  return "https://" . $collectiveDomain . "/allowaccess?client_id=" . WIDEN_CLIENT_REGISTRATION . "&redirect_uri=" . $returnLink;
}

function widendam_api_authenticate($authCode) {
  $collectiveDomain = widendam_variable_get('collectiveDomain');
  $endpoint = 'https://' . $collectiveDomain . '/api/rest/oauth/token';

  $data = "{\n    \"authorization_code\": \"" . $authCode . "\",\n    \"grant_type\" : \"authorization_code\"\n}";
  $auth = 'Basic ' . base64_encode(widendam_drupal_client_registration());

  $options = [
    'method' => 'POST',
    'data' => $data,
    'headers' => [
      'Content-Type' => 'application/json',
      'Authorization' => $auth,
    ],
  ];

  $response = drupal_http_request($endpoint, $options);
  $http_status = $response->code;

  if ($http_status != '200') {
    $error_msg = 'Error Response from Authorization call [' . $http_status . '] ';
    drupal_set_message($error_msg, 'error');
  }

  return json_decode($response->data);
}

function widendam_api_deauthorize() {
  return widendam_api_call("oauth/logout");
}

function widendam_api_search_by_expression($searchTerm, $start, $max, $includeEmbed, $filters = array()) {
  $parms = "?start=" . $start . "&max=" . $max;
  //$parms .= "&filters=IMAGES,VIDEO,AUDIO,PDF";

  if ($includeEmbed)
  {
    $parms .= "&options=embedCodes";
  }

  if (count($filters)) {
    foreach ($filters as $filter_code => $filter) {
      $searchTerm .= '(' . $filter_code . ':' . rawurlencode($filter) . ')';
    }
  }

  return widendam_api_call("asset/search/" . $searchTerm . $parms);
}

function widendam_api_search_by_uuid($uuid, $params = '') {
  return widendam_api_call("asset/uuid/" . $uuid . '?' . $params);
}

function widendam_api_get_embed_codes($uuid) {
  return widendam_api_call("asset/uuid/" . $uuid . "/embed/code");
}

function widendam_api_call($method) {
  $accessToken = widendam_get_access_token();
  $endpoint = widendam_get_endpoint($method);

  if (isset($accessToken) && isset($endpoint)) {
    $auth = 'Bearer ' . $accessToken;

    $options = ['headers' => ['Authorization' => $auth]];

    $response = drupal_http_request($endpoint, $options);
    $http_status = $response->code;

    if ($http_status != '200') {
      $error_msg = 'Error Response from API call [' . $http_status . '] ';
      drupal_set_message($error_msg, 'error');
    }

    return json_decode($response->data);
  }
}

function widendam_api_post_reg_link($assetUuid, $description, $url) {
  $accessToken = widendam_get_access_token();
  $endpoint = widendam_get_endpoint('integrationlink');

  if (isset($accessToken) && isset($endpoint)) {
    $auth = 'Bearer ' . $accessToken;
    $data = "{\n    \"assetUuid\": \"" . $assetUuid . "\",\n    \"description\" : \"" . $description . "\",\n    \"url\" : \"" . $url . "\"\n}";

    $options = [
      'method' => 'POST',
      'data' => $data,
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $auth,
      ],
    ];

    $response = drupal_http_request($endpoint, $options);

    $http_status = $response->code;
    dpm("IntegrationLink POST response" . $http_status);

    // Do nothing with the response, even upon error.
    // We do not want to interrupt user experience for a failed Integration Link registration
  }
}

function widendam_get_endpoint($method) {
  $collectiveDomain = widendam_variable_get('collectiveDomain');
  if (isset($collectiveDomain)) {
    return "https://" . $collectiveDomain . "/api/rest/" . $method;
  }

  drupal_set_message('Widen DAM endpoint must be configured', 'error');
}

function widendam_get_access_token() {
  $token = variable_get('widen_token', '');
  if ($token) {
    return $token;
  }
  drupal_set_message('Not Authorized with Widen DAM', 'error');
}

function widendam_has_access_token() {
  $token = variable_get('widen_token', '');
  return (isset($token) && (strlen($token) != 0));
}

function widendam_drupal_client_registration() {
  // Client Registration with Widen Collective is the same for all Drupal modules
  return WIDEN_CLIENT_REGISTRATION . ':' . 'cc8af29607993c0f9590f4ee0775046a4dfc4eeb';
}

function widendam_api_v2_search_by_expression($searchTerm, $start, $max, $includeEmbed, $filters = [], $scroll = TRUE) {
  $parms = "&offset=" . $start . "&limit=" . $max . "&expand=asset_properties,file_properties,embeds,thumbnails,metadata,metadata_info,metadata_vocabulary,security";

  if ($scroll) {
    $parms .= '&scroll=true';
  }
  if (count($filters)) {
    foreach ($filters as $filter_code => $filter) {
      $searchTerm .= '(' . $filter_code . ':' . rawurlencode($filter) . ')';
    }
  }

  return widendam_api_v2_call("assets/search/?query=" . $searchTerm . $parms);
}

function widendam_api_search_scroll($scroll_id) {
  $parms = "?expand=asset_properties,file_properties,embeds,thumbnails,metadata,metadata_info,metadata_vocabulary,security&scroll_id=" . $scroll_id;

  return widendam_api_v2_call("assets/search/scroll" . $parms);
}

function widendam_api_v2_get_endpoint($method) {
  return 'https://api.widencollective.com/v2/' . $method;
}

function widendam_api_v2_search_by_uuid($uuid, $params = '') {
  if (empty($params)) {
    $params = "expand=asset_properties,file_properties,embeds,thumbnails,metadata,metadata_info,metadata_vocabulary,security";
  }
  return widendam_api_v2_call("assets/" . $uuid . '?' . $params);
}

function widendam_api_v2_call($method) {
  $accessToken = widendam_get_access_token();
  $endpoint = widendam_api_v2_get_endpoint($method);

  if (isset($accessToken) && isset($endpoint)) {
    $auth = 'Bearer ' . $accessToken;

    $options = ['headers' => ['Authorization' => $auth]];

    $response = drupal_http_request($endpoint, $options);
    $http_status = $response->code;

    if ($http_status != '200') {
      $error_msg = 'Error Response from API call [' . $http_status . '] ';
      drupal_set_message($error_msg, 'error');
    }

    return json_decode($response->data);
  }
}