<?php

function widendam_admin()
{
    $build = array();
    $build['form'] = drupal_get_form('widendam_admin_form');
    return $build;
}

function widendam_admin_form()
{
    $form['collective_domain'] = array(
        '#type' => 'textfield',
        '#title' => t('Widen DAM Domain'),
        '#required' => TRUE,
        '#default_value' => widendam_variable_get('collectiveDomain'),
        '#description' => t('example: demo.widencollective.com'),
    );
    
    $form['widen_client_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Client ID'),
        '#required' => TRUE,
        '#default_value' => variable_get('widen_client_id', '167d00b8e36e37dc47d4fb9876192c003f6d8e0a.app.widen.com'),
    );

    $form['widen_client_secret'] = array(
        '#type' => 'textfield',
        '#title' => t('Client Secret'),
        '#required' => TRUE,
        '#default_value' => variable_get('widen_client_secret', 'cc8af29607993c0f9590f4ee0775046a4dfc4eeb'),
    );

    $form['widen_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Authentication token'),
        '#default_value' => variable_get('widen_token', ''),
    );

    $form['widen_user'] = array(
        '#type' => 'textfield',
        '#title' => t('Authenticated user'),
        '#default_value' => variable_get('widen_user', ''),
    );
    
    $form['#submit'][] = 'widendam_admin_form_submit';

    return system_settings_form($form);
}


function widendam_admin_form_submit($form, &$form_state)
{
    $collective_domain = $form_state['values']['collective_domain'];
    if (isset($collective_domain))
    {
        widendam_variable_set('collectiveDomain', $collective_domain);
    }

    widendam_validate_collective_ping_80();
    widendam_validate_collective_ping_443();
    $ok = widendam_validate_rest_post();

    if ($ok)
    {
        drupal_set_message('Widen DAM configuration validated and settings saved.');
        widendam_variable_set('configValidated', 'ok');
    }
    else
    {
        drupal_set_message('Error configuring Widen DAM module', 'error');
        widendam_variable_set('configValidated', null);
    }
        
    if (variable_get('widen_token') == '' || variable_get('widen_client_id', '') == '' || variable_get('widen_client_secret', '') == '') {
        $form_state['redirect'] = 'admin/content/widen/authenticate/refresh';
    }
    else {
      $form_state['rebuild'] = TRUE;
    }
    
}


/**
 * 
 *  Function for Widen Authendication Call back to Allow access
 *  Obtaining an Authorization Code
 */
function widendam_admin_token_refresh() {
    $url = 'https://' . widendam_variable_get('collectiveDomain') . '/allowaccess';
    $query = array(
        'client_id' => variable_get('widen_client_id', ''),
        'redirect_uri' => url('admin/content/widen/authenticate/callback', array('absolute' => TRUE,)),
        'response_type' => 'code',
        'state' => drupal_get_token('widen authentication'),
    );
    $redirect = url($url, array('query' => $query));
    drupal_goto($redirect);
}

/**
 * Function to Check Authendications and returns Widen Token and User 
 */
function widendam_admin_token_callback() {
    if (empty($_GET['state']) || $_GET['state'] != drupal_get_token('widen authentication')) {
        drupal_access_denied();
        exit;
    }
    if (empty($_GET['code'])) {
        drupal_access_denied();
        exit;
    }

    $url = 'https://' . widendam_variable_get('collectiveDomain') . '/api/rest/oauth/token';
    $options = array(
        'method' => 'POST',
        'data' => drupal_json_encode(array(
            'authorization_code' => $_GET['code'],
            'grant_type' => 'authorization_code',
        )),
        'headers' => array(
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . base64_encode(variable_get('widen_client_id', '') . ':' . variable_get('widen_client_secret', '')),
        ),
    );

    $response = drupal_http_request($url, $options);
    if ($response->code != '200') {
        // TODO: Error handling
    } else {
        $data = drupal_json_decode($response->data);
        variable_set('widen_token', $data['access_token']);
        variable_set('widen_user', $data['username']);
    }
    
    drupal_goto('admin/config/content/widen');
}

function widendam_validate_collective_ping_443()
{
    $collectiveDomain = widendam_variable_get('collectiveDomain');
    $endpoint = 'https://' . $collectiveDomain . '/collective.ping';

    $response = drupal_http_request($endpoint);
    $http_status = $response->code;

    if ($http_status == '200')
    {
        drupal_set_message('Validating Widen DAM Domain (https): OK ' . $response->data);
        return true;
    }

    drupal_set_message('Validating Widen DAM Domain (https): ' . $http_status, 'error');
    return false;
}

function widendam_validate_collective_ping_80()
{
    $collectiveDomain = widendam_variable_get('collectiveDomain');
    $endpoint = 'http://' . $collectiveDomain . '/collective.ping';

    $response = drupal_http_request($endpoint);
    $http_status = $response->code;

    if ($http_status == '200')
    {
        drupal_set_message('Validating Widen DAM Domain (http): OK ' . $response->data);
        return true;
    }

    drupal_set_message('Validating Widen DAM Domain (http): ' . $http_status, 'error');
    return false;
}

function widendam_validate_rest_post()
{
    $collectiveDomain = widendam_variable_get('collectiveDomain');
    $endpoint = 'https://' . $collectiveDomain . '/api/rest/oauth/prelogin';

    $options = array('method' => 'POST');

    $response = drupal_http_request($endpoint, $options);
    $http_status = $response->code;


    if ($http_status == '200')
    {
        drupal_set_message('Validating API call: OK');
        return true;
    }

    drupal_set_message('Validating API call: ' . $http_status, 'error');
    return false;
}


