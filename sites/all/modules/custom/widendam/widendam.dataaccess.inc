<?php

define('WIDEN_VARIABLE_NAMESPACE', 'widendam__');
define('WIDEN_PAGE_SIZE_DEFAULT', 20);

function widendam_variable_get($name)
{
    // Namespace all variables
    $variable_name = WIDEN_VARIABLE_NAMESPACE . $name;
    return variable_get($variable_name);
}

function widendam_variable_set($name, $value)
{
    $variable_name = WIDEN_VARIABLE_NAMESPACE . $name;
    return variable_set($variable_name, $value);
}

function widendam_variable_del($name)
{
    $variable_name = WIDEN_VARIABLE_NAMESPACE . $name;
    variable_del($variable_name);
}

function widendam_is_configured()
{
    $collectiveDomain = widendam_variable_get('collectiveDomain');
    $configValidated = widendam_variable_get('configValidated');
    if (!isset($collectiveDomain) || !isset($configValidated))
    {
        return false;
    }

    return (strlen($collectiveDomain) != 0);
}
