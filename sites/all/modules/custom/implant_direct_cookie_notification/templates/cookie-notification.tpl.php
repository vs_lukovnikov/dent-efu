<div id="cookie-notification" class="implant-direct-overlay">
    <div class="implant-gdpr-overlay">
      <div class="container">
        <div class="implant-gdpr-inner-text-wrapper">
          <?php if ($overlay_title): ?>
              <h3><?php print $overlay_title; ?></h3>
          <?php endif ?>
          <?php if ($overlay_message): ?>
              <p><?php print $overlay_message; ?></p>
          <?php endif ?>
        </div>
        <div class="implant-gdpr-inner-btn-wrapper">
          <?php if ($overlay_btn): ?>
              <button class="btn btn-custom"><?php print $overlay_btn; ?></button>
          <?php endif ?>
        </div>
    </div>
    </div>

    <div class="cookie-notification implant-direct-popup">
        <div class="cookie-notification__inner">
            <div class="cookie-notification__text">
              <?php if ($title): ?>
                  <h3><?php print $title; ?></h3>
              <?php endif ?>
              <?php if ($message): ?>
                  <p><?php print $message; ?></p>
              <?php endif ?>
            </div>
          <?php if ($form): ?>
            <?php print render($form); ?>
          <?php endif ?>
        </div>
    </div>
</div>

<noscript>
    <div class="implant-direct-overlay">
        <div class="implant-gdpr-overlay">
            <div class="implant-gdpr-inner-text-wrapper">
              <?php if ($overlay_title): ?>
                  <h3><?php print $overlay_title; ?></h3>
              <?php endif ?>
              <?php if ($overlay_message): ?>
                  <p><?php print $overlay_message; ?></p>
              <?php endif ?>
            </div>
            <div class="implant-gdpr-inner-btn-wrapper">
              <?php if ($overlay_btn): ?>
                  <button class="btn btn-custom"><?php print $overlay_btn; ?></button>
              <?php endif ?>
            </div>
        </div>

        <div class="cookie-notification implant-direct-popup">
            <div class="cookie-notification__inner">
                <div class="cookie-notification__text">
                  <?php if ($title): ?>
                      <h3><?php print $title; ?></h3>
                  <?php endif ?>
                  <?php if ($message): ?>
                      <p><?php print $message; ?></p>
                  <?php endif ?>
                </div>
              <?php if ($form): ?>
                <?php print render($form); ?>
              <?php endif ?>
            </div>
        </div>
    </div>
</noscript>
