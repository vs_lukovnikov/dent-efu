(function($) {
    Drupal.behaviors.implant_direct_cookie_notification = {
        attach: function () {
            // Show cookie compliance message if the cookie is not set.
            if (document.cookie.indexOf('SESScookienotificationdismissed=') === -1) {
                $('#cookie-notification').show();
                $('#cookie-notification .implant-gdpr-overlay').slideDown('slow');

                $('#cookie-notification .implant-gdpr-overlay button').click(function () {
                    $('#cookie-notification .implant-gdpr-overlay').slideUp('slow');
                    $('#cookie-notification .implant-direct-popup').show();
                });
            }
        }
    }
}(jQuery));
