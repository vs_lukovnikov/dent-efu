<?php

/**
 * Constants used in module.
 */
const KERRDENTAL_IMAGE_TRACKING_DIRECTORY = 'public://image_tracking/';

/**
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function kerrdental_widen_image_tracking_form($form, &$form_state) {
  $form['step_1'] = [
    '#type' => 'fieldset',
    '#title' => t('Step 1'),
  ];

  $form['step_1']['report_file_usage'] = [
    '#type' => 'submit',
    '#value' => t('Generate report of files usage'),
    '#submit' => ['kerrdental_widen_image_tracking_generate_report_file_usage'],
  ];

  $form['step_1']['markup'] = [
    '#markup' => '<br>' . t('The script will find all places of image usage and write them to the table.')
  ];

  $report_generated = variable_get('kerrdental_widen_image_file_usage_report_generated', FALSE);

  $form['step_2'] = [
    '#type' => 'fieldset',
    '#title' => t('Step 2')
  ];

  $form['step_2']['report_file_tracking'] = [
    '#type' => 'submit',
    '#disabled' => !$report_generated ? TRUE : FALSE,
    '#value' => t('Generate report of files tracking'),
    '#submit' => ['kerrdental_widen_image_tracking_generate_file_tracking_report'],
  ];

  $form['step_2']['markup'] = [
    '#markup' => t('<br> The script will check every image usage on the site and check if it is tracked by Widen sync. 
    There are following statuses: <br> 
    - TRACKED - image is tracked by Widen module <br>
    - ACTIVE - image exists in Widen but is not tracked by Widen module in the site <br>
    - EXPIRED - image exists on the site but expired in Widen <br>
    - NOT_FOUND - image exists on the site but not found in Widen <br><br>')
  ];

  $file_tracking_report_generated = variable_get('kerrdental_widen_image_file_tracking_report_generated', FALSE);
  if ($file_tracking_report_generated) {
    $form['step_2']['markup_2'] = [
      '#markup' => t('Download reports: <br> 
    - TRACKED files - ' . l('Download', file_create_url('public://image_tracking/image_tracking_report_tracked.csv')) . ' <br>
    - ACTIVE files - ' . l('Download', file_create_url('public://image_tracking/image_tracking_report_active.csv')) . ' <br>
    - EXPIRED files - ' . l('Download', file_create_url('public://image_tracking/image_tracking_report_expired.csv')) . ' <br>
    - NOT_FOUND files - ' . l('Download', file_create_url('public://image_tracking/image_tracking_report_not_found.csv')))
    ];
  }

  $form['step_3'] = [
    '#type' => 'fieldset',
    '#title' => t('Step 3')
  ];

  $form['step_3']['report'] = [
    '#type' => 'submit',
    '#disabled' => $file_tracking_report_generated ? FALSE : TRUE,
    '#value' => t('Track ACTIVE images to Widen sync'),
    '#submit' => ['kerrdental_widen_image_tracking_track_active_images'],
  ];

  $form['step_3']['markup'] = [
    '#markup' => '<br>' . t('The script will add images to Widen sync process. ')
  ];

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function kerrdental_widen_image_tracking_generate_report_file_usage($form, &$form_state) {
  variable_set('kerrdental_widen_image_file_usage_report_generated', FALSE);
  // prepare array of operation for Batch API
  db_truncate(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE)->execute();
  $field_types = ['image', 'text', 'text_long'];
  $operations = [];
  foreach ($field_types as $field_type) {
    $fields_list = field_read_fields(['type' => $field_type]);
    if (!empty($fields_list)) {
      $operations[] = [
        'kerrdental_widen_image_tracking_report_batch_iteration',
        [$fields_list, $field_type]
      ];
    }
  }

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_image_tracking_file_usage_report_batch_finished',
    'file' => drupal_get_path('module', 'kerrdental_widen_image_tracking') . '/kerrdental_widen_image_tracking.admin.inc',
    'title' => t('Widen sync new assets processing'),
    'progress_message' => t(''),
  ));
}

/**
 * @param $fields_list
 * @param $field_type
 * @param $context
 *
 * @throws \Exception
 */
function kerrdental_widen_image_tracking_report_batch_iteration($fields_list, $field_type, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['items'] = $fields_list;
    if (isset($context['sandbox']['items']['field_resource_image'])) {
      unset($context['sandbox']['items']['field_resource_image']);
    }
    $first_element = reset(array_splice($context['sandbox']['items'], 0, 1));
    $context['sandbox']['current_field'] = $first_element['field_name'];
    $query = db_select('field_data_' . $first_element['field_name'], $first_element['field_name']);
    $result = $query->countQuery()->execute()->fetchField();
    $context['sandbox']['current_field_max'] = (int)$result;
    $max = (int)$result;
    foreach ($context['sandbox']['items'] as $key => $value) {
      $query = db_select('field_data_' . $key, $key);
      $result = $query->countQuery()->execute()->fetchField();
      $max += (int)$result;
    }
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_offset'] = 0;
    $context['sandbox']['max'] = $max;
    $context['sandbox']['current_progress'] = 0;
  }
  $limit = 50;

  if (!empty($context['sandbox']['current_field'])) {
    $current_field = $context['sandbox']['current_field'];
    $query = db_select('field_data_' . $context['sandbox']['current_field'], 'f');

    if ($field_type === 'text' || $field_type === 'text_long') {
      $query->fields('f', ['entity_id', 'entity_type', $context['sandbox']['current_field'] . '_value']);
    } elseif ($field_type === 'image') {
      $query->fields('f', ['entity_id', 'entity_type', $context['sandbox']['current_field'] . '_fid']);
    }

    $query->range($context['sandbox']['current_progress'], $limit);
    $result = $query->execute();
    $db_insert = db_insert(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE)
      ->fields(['fid', 'entity_id', 'entity_type', 'field_name', 'filename', 'uuid', 'status']);

    if ($field_type !== 'image') {
      foreach ($result as $value) {
        $entity_id = $value->entity_id;
        $entity_type = $value->entity_type;
        $html = $value->{$context['sandbox']['current_field'] . '_value'};
        $doc = new DOMDocument();
        @$doc->loadHTML($html);
        $tags = $doc->getElementsByTagName('img');

        if (isset($tags->length) && $tags->length > 0) {
          foreach ($tags as $tag) {
            $img_src = $tag->getAttribute('src');
            $url_parts = explode('/', $img_src);
            $filename = $url_parts[count($url_parts) - 1];
            $filename = strtok($filename, '?');
            $search = 'widen';

            if(isset($url_parts[2]) && !preg_match("/{$search}/i", $url_parts[2])) {
              $query = db_select('file_managed', 'f');
              $query->fields('f', ['fid']);
              $query->condition('f.uri', '%' . db_like($filename), 'LIKE');
              $files = $query->execute()->fetchCol(0);

              if (!empty($files)) {
                foreach ($files as $file) {
                  $query = db_select('widen_images', 'w');
                  $query->fields('w', ['fid', 'uuid']);
                  $query->condition('fid', $file);
                  $result = $query->execute()->fetchAllKeyed(0,1);

                  if (isset($result[$file])) {
                    $record = _kerrdental_widen_image_tracking_create_record($file, $entity_id, $entity_type, $current_field, $filename, $result[$file]);
                  } else {
                    $record = _kerrdental_widen_image_tracking_create_record($file, $entity_id, $entity_type, $current_field, $filename);
                  }
                  $db_insert->values($record);
                }
              }
            }
          }
        }
        $context['sandbox']['progress']++;
        $context['sandbox']['current_progress']++;
        $context['message'] = t('Processing field %field. Done %progress of %count', array(
            '%progress' => $context['sandbox']['progress'],
            '%count' => $context['sandbox']['max'],
            '%field' => $context['sandbox']['current_field']
          )
        );
      }
    } else {
      foreach ($result as $value) {
        $entity_id = $value->entity_id;
        $entity_type = $value->entity_type;
        $file_id = $value->{$context['sandbox']['current_field'] . '_fid'};
        $file = file_load($file_id);
        if ($file) {
          $query = db_select('widen_images', 'w');
          $query->fields('w', ['fid', 'uuid']);
          $query->condition('fid', $file_id);
          $result = $query->execute()->fetchAllKeyed(0,1);

          if (isset($result[$file_id])) {
            $record = _kerrdental_widen_image_tracking_create_record($file_id, $entity_id, $entity_type, $current_field, $file->filename, $result[$file_id]);
          } else {
            $record = _kerrdental_widen_image_tracking_create_record($file_id, $entity_id, $entity_type, $current_field, $file->filename);
          }
          $db_insert->values($record);
        }

        $context['sandbox']['progress']++;
        $context['sandbox']['current_progress']++;
        $context['message'] = t('Processing field %field. Done %progress of %count', array(
            '%progress' => $context['sandbox']['progress'],
            '%count' => $context['sandbox']['max'],
            '%field' => $context['sandbox']['current_field']
          )
        );
      }
    }
    $db_insert->execute();
  }

  if ($context['sandbox']['current_progress'] < $context['sandbox']['current_field_max']) {
    $context['sandbox']['current_offset'] += $limit;
  } else {
    if (!empty($context['sandbox']['items'])) {
      $first_element = reset(array_splice($context['sandbox']['items'], 0, 1));
      $context['sandbox']['current_field'] = $first_element['field_name'];
      $query = db_select('field_data_' . $first_element['field_name'], $first_element['field_name']);
      $result = $query->countQuery()->execute()->fetchField();
      $context['sandbox']['current_field_max'] = (int)$result;
      $context['sandbox']['current_offset'] = 0;
      $context['sandbox']['current_progress'] = 0;
    }
  }


  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Preparing values to be inserted to db.
 *
 * @param $file_id
 * @param $entity_id
 * @param $entity_type
 * @param $current_field
 * @param $filename
 * @param string $uuid
 *
 * @return array
 */
function _kerrdental_widen_image_tracking_create_record($file_id, $entity_id, $entity_type, $current_field, $filename, $uuid = '') {
  $record = [
    'fid' => $file_id,
    'entity_id' => $entity_id,
    'entity_type' => $entity_type,
    'field_name' => $current_field,
    'filename' => $filename,
    'uuid' => $uuid ? $uuid : '',
    'status' => $uuid ? KERRDENTAL_IMAGE_TRACKING_STATUS_TRACKED : ''
  ];

  return $record;
}

/**
 * Finish callback for Batch
 */
function kerrdental_widen_image_tracking_file_usage_report_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Finished without error.');
    variable_set('kerrdental_widen_image_file_usage_report_generated', TRUE);
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}

/**
 * Finish callback for Batch
 */
function kerrdental_widen_image_tracking_file_tracking_report_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Finished without error.');
    variable_set('kerrdental_widen_image_file_tracking_report_generated', TRUE);
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}

/**
 * @param $form
 * @param $form_state
 */
function kerrdental_widen_image_tracking_generate_file_tracking_report($form, &$form_state) {
  variable_set('kerrdental_widen_image_file_tracking_report_generated', FALSE);

  $operations[] = [
    'kerrdental_widen_image_tracking_batch_iteration',
    []
  ];

  $operations[] = [
    'kerrdental_widen_image_tracking_csv_report_generation',
    [KERRDENTAL_IMAGE_TRACKING_STATUS_TRACKED]
  ];

  $operations[] = [
    'kerrdental_widen_image_tracking_csv_report_generation',
    [KERRDENTAL_IMAGE_TRACKING_STATUS_EXPIRED]
  ];

  $operations[] = [
    'kerrdental_widen_image_tracking_csv_report_generation',
    [KERRDENTAL_IMAGE_TRACKING_STATUS_ACTIVE]
  ];

  $operations[] = [
    'kerrdental_widen_image_tracking_csv_report_generation',
    [KERRDENTAL_IMAGE_TRACKING_STATUS_NOT_FOUND]
  ];

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_image_tracking_file_tracking_report_batch_finished',
    'file' => drupal_get_path('module', 'kerrdental_widen_image_tracking') . '/kerrdental_widen_image_tracking.admin.inc',
    'title' => t('Widen sync new assets processing'),
    'progress_message' => t(''),
  ));
}

/**
 * @param $context
 */
function kerrdental_widen_image_tracking_batch_iteration(&$context) {
  if (empty($context['sandbox'])) {
    $query = db_select(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE, 'w');
    $query->condition('w.uuid', '');
    $query->condition('w.status', '');
    $result = $query->countQuery()->execute()->fetchField();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $result;
    $context['sandbox']['processed_items'] = [];
  }

  $precessed_items = $context['sandbox']['processed_items'];
  $query = db_select(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE, 'w');
  $query->fields('w');
  $query->condition('w.uuid', '');
  $query->condition('w.status', '');
  if (count($precessed_items)) {
    $query->condition('w.rid', $precessed_items, 'NOT IN');
  }
  $query->groupBy('w.filename');
  $query->range(0, 1);
  $results = $query->execute()->fetchAllAssoc('rid');

  foreach ($results as $rid => $result) {
    $filters = $filters_save = array();

    // PRE-FILTERS
    $prefilters = kerrdental_widen_prefilters_array();
    foreach ($prefilters as $prefilter) {
      if ($filter_terms = variable_get('kerrdental_widen_prefilters_images_' . $prefilter['key'])) {
        $type_filters = array();
        $terms = taxonomy_term_load_multiple($filter_terms);
        foreach ($terms as $t) {
          $type_filters[] = '{'. $t->name . '}';
        }
        $filters[$prefilter['key']] = implode(' or ', $type_filters);
      }
    }

    $filters['fn'] = $result->filename;

    $response = widendam_api_v2_search_by_expression('', 0, 10, true, $filters, FALSE);
    if ($response->total_count) {
      foreach ($response->items as $asset) {
        if ($asset->released_and_not_expired) {
          $db_update = db_update(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE);
          $db_update->fields([
            'uuid' => $asset->id,
            'status' => KERRDENTAL_IMAGE_TRACKING_STATUS_ACTIVE
          ]);
          $db_update->condition('rid', $rid);
          $db_update->execute();
        } else {
          $db_update = db_update(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE);
          $db_update->fields([
            'uuid' => $asset->id,
            'status' => KERRDENTAL_IMAGE_TRACKING_STATUS_EXPIRED
          ]);
          $db_update->condition('rid', $rid);
          $db_update->execute();
        }
      }
    } else {
      $filename = _kerrdental_widen_image_tracking_filename_remove_suffix($result->filename);
      if ($filename != $result->filename) {
        $filters['fn'] = $filename;
        $response = widendam_api_v2_search_by_expression('', 0, 10, true, $filters, FALSE);
        if ($response->total_count) {
          foreach ($response->items as $asset) {
            if ($asset->released_and_not_expired) {
              $db_update = db_update(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE);
              $db_update->fields([
                'uuid' => $asset->id,
                'status' => KERRDENTAL_IMAGE_TRACKING_STATUS_ACTIVE
              ]);
              $db_update->condition('rid', $rid);
              $db_update->execute();
            } else {
              $db_update = db_update(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE);
              $db_update->fields([
                'uuid' => $asset->id,
                'status' => KERRDENTAL_IMAGE_TRACKING_STATUS_EXPIRED
              ]);
              $db_update->condition('rid', $rid);
              $db_update->execute();
            }
          }
        } else {
          $db_update = db_update(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE);
          $db_update->fields([
            'status' => KERRDENTAL_IMAGE_TRACKING_STATUS_NOT_FOUND
          ]);
          $db_update->condition('rid', $rid);
          $db_update->execute();
        }
      } else {
        $db_update = db_update(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE);
        $db_update->fields([
          'status' => KERRDENTAL_IMAGE_TRACKING_STATUS_NOT_FOUND
        ]);
        $db_update->condition('rid', $rid);
        $db_update->execute();
      }
    }
    $context['sandbox']['processed_items'][] = $rid;
    $context['sandbox']['progress']++;
    $context['message'] = t('Done %progress of %count', array(
        '%progress' => $context['sandbox']['progress'],
        '%count' => $context['sandbox']['max']
      )
    );
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * @param $filename
 *
 * @return string
 */
function _kerrdental_widen_image_tracking_filename_remove_suffix($filename) {
  $filename_parts = explode('.', $filename);
  $filename = $filename_parts[0];
  $extention = $filename_parts[1];
  $filename_parts = explode('_', $filename);
  if (count($filename_parts) > 1) {
    unset($filename_parts[count($filename_parts) - 1]);
  }
  $filename = implode('_', $filename_parts);
  $filename .= '.' . $extention;

  return $filename;
}

/**
 * @param $status
 * @param $context
 */
function kerrdental_widen_image_tracking_csv_report_generation($status, &$context) {
  $file_directory = KERRDENTAL_IMAGE_TRACKING_DIRECTORY;
  $file_destination = $file_directory . 'image_tracking_report_' . $status . '.csv';

  if (empty($context['sandbox'])) {
    $query = db_select(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE, 'w');
    $query->condition('w.status', $status);
    $result = $query->countQuery()->execute()->fetchField();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $result;

    // prepare array of operation for Batch API
    file_prepare_directory($file_directory, FILE_CREATE_DIRECTORY);
    file_save_data('', $file_destination, FILE_EXISTS_REPLACE);
    $csv_file = fopen($file_destination, 'a') or die('Cant open file!');
    fputcsv($csv_file, [
      t('File ID'),
      t('Entity ID'),
      t('Entity type'),
      t('Field name'),
      t('Filename'),
      t('UUID'),
      t('Node url')
    ]);
  } else {
    $csv_file = fopen($file_destination, 'a') or die('Cant open file!');
  }

  $query = db_select(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE, 'w');
  $query->fields('w', ['rid', 'fid', 'entity_id', 'entity_type', 'field_name', 'filename', 'uuid']);
  $query->condition('w.status', $status);
  $query->range($context['sandbox']['progress'], 100);
  $result = $query->execute()->fetchAllAssoc('rid');

  foreach ($result as $rid => $row) {
    $node_url = '';
    global $base_url;
    if ($row->entity_type === 'node') {
      $node_url = $base_url . '/node/' . $row->entity_id;
    } elseif ($row->entity_type === 'field_collection_item') {
      $bundle_query = db_select('field_data_' . $row->field_name, 'b');
      $bundle_query->fields('b', ['bundle']);
      $bundle_query->condition('b.entity_id', $row->entity_id);
      $bundle = $bundle_query->execute()->fetchField(0);
      $node_query = db_select('field_data_' . $bundle, 'fc');
      $node_query->fields('fc', ['entity_id']);
      $node_query->condition('fc.' . $bundle . '_value', $row->entity_id);
      $node_id = $node_query->execute()->fetchField(0);
      if ($node_id) {
        $node_url = $base_url . '/node/' . $node_id;
      }
    }
    fputcsv(
      $csv_file,
      [
        $row->fid,
        $row->entity_id,
        $row->entity_type,
        $row->field_name,
        $row->filename,
        $row->uuid,
        $node_url
      ], ',', '"');
    $context['sandbox']['progress']++;
  }

  fclose($csv_file);

  $context['message'] = t('Generating csv report of %status images. Done %progress of %count', array(
      '%progress' => $context['sandbox']['progress'],
      '%count' => $context['sandbox']['max'],
      '%status' => $status
    )
  );

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * @param $form
 * @param $form_state
 */
function kerrdental_widen_image_tracking_track_active_images($form, &$form_state) {
  $files_list = kerrdental_widen_image_tracking_get_untracked_files_list();

  $operations[] = [
    'kerrdental_widen_image_tracking_track_image_iteration',
    [$files_list],
  ];

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_image_tracking_track_image_batch_finished',
    'file' => drupal_get_path('module', 'kerrdental_widen_image_tracking') . '/kerrdental_widen_image_tracking.admin.inc',
    'title' => t('Widen sync new assets processing'),
    'progress_message' => t(''),
  ));
}

/**
 * @param $files_list
 * @param $context
 */
function kerrdental_widen_image_tracking_track_image_iteration($files_list, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($files_list);
    $context['sandbox']['items'] = $files_list;
  }

  $file = array_shift($context['sandbox']['items']);

  $parms = "expand=asset_properties,file_properties,embeds,thumbnails,metadata,metadata_info,metadata_vocabulary,security";
  $asset = widendam_api_v2_search_by_uuid($file->uuid, $parms);
  if (isset($asset->filename)) {
    $result = db_query("SELECT fid FROM {widen_images} WHERE fid = :fid", [':fid' => $file->fid]);
    $count = $result->rowCount();

    if (!$count) {
      $extension = explode('.', $file->filename)[1];
      if ($extension == 'png') {
        $url = $asset->embeds->Originalsizepng->url;
      }
      else {
        $url = $asset->embeds->{'OriginalWidth&Height'}->url;
      }
      kerrdental_widen_insert_image($asset, $url, $file);
    }

    $db_update = db_update(KERRDENTAL_WIDEN_IMAGE_TRACKING_TABLE);
    $db_update->fields([
      'status' => KERRDENTAL_IMAGE_TRACKING_STATUS_TRACKED,
    ]);
    $db_update->condition('filename', $file->filename);
    $db_update->execute();
  }

  $context['sandbox']['progress']++;

  $context['message'] = t('Done %progress of %count', [
      '%progress' => $context['sandbox']['progress'],
      '%count' => $context['sandbox']['max']
    ]
  );

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Finish callback for Batch
 */
function kerrdental_widen_image_tracking_track_image_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Finished without error.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}