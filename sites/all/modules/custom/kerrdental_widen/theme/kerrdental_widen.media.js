(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.kerrdental_widen = {
    attach: function(context, settings) {
    
      $('div.media-widget a.browse').click(function() {
        var id = $(this).parent('div.media-widget').attr('id');
        Drupal.settings.kerrdental_widen = {};
        Drupal.settings.kerrdental_widen.media_id = id;
        Drupal.settings.kerrdental_widen.filters = $('#edit-field-image-filters-und-0-value').val();
      });
    
    }
  };

  $(function() {
    
    if ($('body').hasClass('page-media-browser')) {
      $('#widendam-results-form .widen_search_result .form-type-select').hide();
    }
    
    // show form again on search results page
    $('#widen-search-box-show').click(function() {
      $('#widen-search-boxer').slideToggle();
      $(this).hide();
    });
    
    // save filters on search submit
    if (typeof Drupal.settings.widendam_filters != 'undefined') {
      window.parent.jQuery('#edit-field-image-filters-und-0-value').val(Drupal.settings.widendam_filters);
    } else {
      if (window.parent.jQuery('#edit-field-image-filters-und-0-value').length > 0 && jQuery('#widen-search-boxer').length > 0) {
        var image_filters = window.parent.jQuery('#edit-field-image-filters-und-0-value').val();
        if (image_filters) {
          var filters = $.parseJSON(image_filters);
          if (filters) {
            $.each(filters, function(index, value) {
              if ($('input#edit-'+index).length > 0) {
                $('input#edit-'+index).val(value);
              }
              if ($('select#edit-'+index).length > 0) {
                $.each(value, function(value_index, value_value) {
                  $('select#edit-'+index+' option[value='+value_value+']').attr('selected','selected');
                });
              }
            });
          }
        }
      }
    }
    
  });

})(jQuery, Drupal, this, this.document);

function kerrdental_widen_embedRef(uuid, code) { 
  jQuery('.ui-dialog').remove();
  var parts = code.split('"');
  var url = parts[1];
  jQuery.ajax({
    url: Drupal.settings.basePath + 'ajax/kerrdental-widen/save-asset',
    type: 'post',
    data: 'uuid=' + encodeURIComponent(uuid) + '&url=' + encodeURIComponent(url) + '&media=true',
    success: function(r){
      if (r.status == 'success') {
        var id = Drupal.settings.kerrdental_widen.media_id;       
        jQuery('#'+id+' .fid').val(r.fid);
        jQuery('#'+id+' .preview').html(r.preview);  
        //Added Alt Tag
        try {
            jQuery('#'+id+'-alt').val(r.alttag);
        }
        catch(err) {  } 
        
        jQuery('#'+id+' .remove').show();
        jQuery('#'+id+' .attach').trigger('mousedown');
        return;
      } else if (r.status == 'error') {
        alert(r.message);
      }
    }
  });
}
