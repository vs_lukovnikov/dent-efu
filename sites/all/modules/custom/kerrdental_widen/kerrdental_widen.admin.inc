<?php
/**
 * @file
 */

function kerrdental_widen_admin_test_form($form, &$form_state) {
  $form = [];
  $form['uuid'] = [
    '#title' => 'UUID of Widen file',
    '#type' => 'textfield',
    '#required' => TRUE,
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Submit',
  ];

  return $form;
}

function kerrdental_widen_admin_test_form_submit($form, &$form_state) {
  $uuid = $form_state['values']['uuid'];
  $parms = "expand=asset_properties,file_properties,embeds,thumbnails,metadata,metadata_info,metadata_vocabulary,security";
  $return = widendam_api_search_by_uuid($uuid, $parms);
  if (module_exists('devel')) {
    dpm($return);
  }
  else {
    drupal_set_message('<pre>' . print_r($return, TRUE) . '</pre>');
  }
}

function kerrdental_get_exec_time($time_start) {
  $seconds = round((microtime(true) - $time_start), 1);

  return "$seconds seconds";
}

function kerrdental_widen_admin_sync_by_nodeid_form($form, &$form_state) {
  return [
    'nodeid' => [
      '#title' => t('Node id of Widen resource asset'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => t("You can set single nodeid or multiple nodeids separated by comma(WITHOUT SPACES)")
    ],
    'submit' => [
      '#type' => 'submit',
      '#value' => 'Submit',
    ],
  ];
}

function kerrdental_widen_admin_sync_by_nodeid_form_submit($form, &$form_state) {
  $nids = explode(',', $form_state['values']['nodeid']);

  if (!empty($nids)) {
    global $base_url;
    $nodes = node_load_multiple($nids);

    if (!empty($nodes) && is_array($nodes)) {
      foreach ($nodes as $node) {
        $uuid = $node->field_resource_uuid['und'][0]['value'];
        $asset = widendam_api_call("asset/uuid/" . $uuid);
        $embed_codes = widendam_api_call("asset/uuid/" . $uuid . "/embed/code");
        $asset->embedCodes = $embed_codes->embedCodes;
        kerrdental_widen_set_asset($node, $asset);
        $node = node_submit($node);
        node_save($node);

        $nodeurl = $base_url . "/" . drupal_get_path_alias("node/{$node->nid}");

        drupal_set_message("Done, check the <a href='$nodeurl' target='_blank'>{$node->nid} result</a>", $type = 'status');
      }
    }
  }
}

function kerrdental_widen_admin_sync_batch_api_form($form, &$form_state) {
  set_time_limit(0);

  // Full sync fieldset settings
  $last_run_full_stack = variable_get('kerrdental_widen_resource_full_sync_stack', 10);
  $last_run_full = variable_get('kerrdental_widen_resource_full_sync_start', 0);
  $last_run_full_date = variable_get('kerrdental_widen_resource_full_sync_last_run', 0);

  $form['full_sync'] = [
    '#type' => 'fieldset',
    '#title' => t('Widen full sync settings'),
    '#weight' => 0,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];
  $form['full_sync']['full_sync_settings'] = [
    'stack' => [
      '#title' => t('Stack of batch for full sync Widen resource assets'),
      '#type' => 'textfield',
      '#description' => t("Recommended 10 assets per batch on Pantheon"),
      '#default_value' => $last_run_full_stack,
    ],
    'last_run_full' => [
      '#title' => $last_run_full_date ? t('The last run of Widen full sync was ') . $last_run_full_date : t('Widen full sync never started before'),
      '#type' => 'textfield',
      '#description' => $last_run_full ? t("You can continue from last run") : '',
      '#value' => $last_run_full ? $last_run_full : 0,
      '#disabled' => TRUE,
    ],
//    'continue' => [
//      '#type' => 'checkbox',
//      '#title' => t('Continue sync from last run'),
//    ],
    'submit' => [
      '#type' => 'submit',
      '#value' => 'Run Full Sync',
      '#submit' => array('kerrdental_widen_admin_run_full_sync'),
    ],
  ];

  // Sync new assets fieldset settings
  $last_run_new_stack = variable_get('kerrdental_widen_resource_new_sync_stack', 10);
  $last_run_new_date = variable_get('kerrdental_widen_resource_new_sync_last_run', null);

  // get all assets that have been updated since last run
  $filters = [];

  cache_clear_all('kerrdental_widen:all_groups', 'cache');
  // CONSTANT FILTERS
//  $filters['rd'] = '[before ' . date("m/d/Y", strtotime('+1 day')) . ']'; // release date
//  $filters['ed'] = '[after ' . date("m/d/Y", strtotime('-1 day')) . '] OR ed:isempty'; // expiration date
  if (null !== $last_run_new_date){
    $last_run_new_date_widen = date("m/d/Y", strtotime('-1 day', $last_run_new_date));
    $filters['lhd'] = '[after ' . $last_run_new_date_widen . ']'; // last edit date
  } else {
    $filters['lhd'] = '[after 01/01/2018]'; // last edit date
  }

  $max_limit_new = $last_run_new_stack;
  $start_new = 0;

  $result_new = widendam_api_v2_search_by_expression('', $start_new, $max_limit_new, TRUE, $filters);
  $num_results_new = $result_new->total_count;

  $new_format = "d m Y";
  $new_last_run_date = [
    'year' => date("Y", $last_run_new_date),
    'month' => date("n", $last_run_new_date),
    'day' => date("j", $last_run_new_date),
  ];
  $new_last_run_now = [
    'year' => date("Y", time()),
    'month' => date("n", time()),
    'day' => date("j", time()),
  ];

  $form['new_sync'] = [
    '#type' => 'fieldset',
    '#title' => t('Widen sync new assets settings'),
    '#weight' => 1,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $form['new_sync']['new_sync_settings'] = [
    'new_stack' => [
      '#title' => t('Stack of batch for sync new Widen resource assets'),
      '#type' => 'textfield',
      '#description' => t("Recommended 10 assets per batch on Pantheon"),
      '#default_value' => $last_run_new_stack,
    ],
    'new_last_run' => [
      '#type' => 'date',
      '#title' => $last_run_new_date ? t('The last run of Widen sync new assets') : t('Widen sync new assets never started before'),
      '#date_format' => $new_format,
      '#default_value' => $last_run_new_date ? $new_last_run_date : $new_last_run_now,
      '#description' => $num_results_new ? t('There are ') . $num_results_new . t(' new assets can be synced') : '',
    ],
    'new_save_settings' => [
      '#type' => 'submit',
      '#value' => 'Save settings',
      '#submit' => array('kerrdental_widen_admin_save_settings_new_sync'),
    ],
    'new_submit' => [
      '#type' => 'submit',
      '#value' => 'Sync new assets',
      '#submit' => array('kerrdental_widen_admin_run_new_sync'),
    ],
  ];

  if ( module_exists('domain') ) {
    //Full sync Domains only
    $last_run_domain_stack = variable_get('kerrdental_widen_resource_domain_sync_stack', 10);

    $form['domain_sync'] = [
      '#type' => 'fieldset',
      '#title' => t('Widen Full sync (Update Domain Access ONLY) settings'),
      '#weight' => 2,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['domain_sync']['domain_sync_settings'] = [
      'domain_stack' => [
        '#title' => t('Stack of batch for sync domains in Widen resource assets'),
        '#type' => 'textfield',
        '#description' => t("Recommended 10 assets per batch on Pantheon"),
        '#default_value' => $last_run_domain_stack,
      ],
      'domain_submit' => [
        '#type' => 'submit',
        '#value' => 'Run Full Sync Domains',
        '#submit' => array('kerrdental_widen_sync_domain_only'),
      ],
    ];
  }

  //Sync by Node ID
  $form['nodeid_sync'] = [
    '#type' => 'fieldset',
    '#title' => t('Widen sync by Node ID settings'),
    '#weight' => 3,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $form['nodeid_sync']['nodeid_sync_settings'] = [
    'nodeid' => [
      '#title' => t('Widen resource sync by Node ID'),
      '#type' => 'textfield',
      '#description' => t("You can set single Node ID or multiple Node ID's separated by comma (WITHOUT SPACES)"),
    ],
    'nodeid_submit' => [
      '#type' => 'submit',
      '#value' => 'Sync Node',
      '#submit' => array('kerrdental_widen_sync_nodeid'),
    ],
  ];

  //Sync by UUID
  $form['uuid_sync'] = [
    '#type' => 'fieldset',
    '#title' => t('Widen sync by Widen UUID settings'),
    '#weight' => 4,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $form['uuid_sync']['uuid_sync_settings'] = [
    'uuid' => [
      '#title' => t('Widen resource sync by Widen UUID'),
      '#type' => 'textfield',
    ],
    'uuid_submit' => [
      '#type' => 'submit',
      '#value' => 'Sync',
      '#submit' => array('kerrdental_widen_sync_uuid'),
    ],
  ];

  //Sync expired assets -30 days from now
  $form['expired_sync'] = [
    '#type' => 'fieldset',
    '#title' => t('Remove expired assets last 30 days'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $form['expired_sync']['expired_sync'] = [
    'expired_remove' => [
      '#type' => 'submit',
      '#value' => 'Delete expired',
      '#submit' => array('kerrdental_widen_delete_expired_last_month'),
    ],
  ];

  $last_run_img = variable_get('kerrdental_widen_image_sync_timestamp', '1527811200');
  $new_format = "d m Y";
  $img_last_run_date = [
    'year' => date("Y", $last_run_img),
    'month' => date("n", $last_run_img),
    'day' => date("j", $last_run_img),
  ];
  $img_last_run_now = [
    'year' => date("Y", time()),
    'month' => date("n", time()),
    'day' => date("j", time()),
  ];

  //test expired images
  $form['expired_images'] = [
    '#type' => 'fieldset',
    '#title' => t('Sync images from content'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $last_run_img_stack = variable_get('kerrdental_widen_sync_images_stack', 10);

  $form['expired_images']['last_run_img'] = [
    'sync_stack' => [
      '#title' => t('Stack of batch for sync images'),
      '#type' => 'textfield',
      '#description' => t("Recommended 10 assets per batch on Pantheon"),
      '#default_value' => $last_run_img_stack,
    ],
    'last_run_img' => [
      '#type' => 'date',
      '#title' => $last_run_img ? t('The last run of Widen images sync') : t('Widen images sync never started before'),
      '#date_format' => $new_format,
      '#default_value' => $last_run_img ? $img_last_run_date : $img_last_run_now,
    ],
  ];

  $form['expired_images']['img_buttons'] = [
    'sync_img' => [
      '#type' => 'submit',
      '#value' => 'Sync images',
//      '#submit' => array('kerrdental_widen_sync_images'),
      '#submit' => array('kerrdental_widen_admin_sync_images'),
    ],
    'expired_img' => [
      '#type' => 'submit',
      '#value' => 'Delete expired images',
      '#submit' => array('kerrdental_widen_remove_expired_images'),
    ],
  ];

  return $form;
}

function kerrdental_widen_admin_sync_images($form, &$form_state) {
  $sync_date = new DateTime();
  $sync_date_array = $form_state['values']['last_run_img'];
  $sync_date->setDate($sync_date_array['year'], $sync_date_array['month'], $sync_date_array['day']);
  variable_set('kerrdental_widen_image_sync_timestamp', $sync_date->getTimestamp());
  $sync_stack = variable_get('kerrdental_widen_sync_images_stack', 10);

  if (!empty($form_state['values']['sync_stack'])) {
    $new_stack = intval($form_state['values']['sync_stack']);

    if ($new_stack !== $sync_stack) {
      $sync_stack = $new_stack;
      variable_set('kerrdental_widen_sync_images_stack', $new_stack);
    }
  }

  $new_timestamp = null;
  if (isset($_REQUEST['last_run_img']) && !empty($_REQUEST['last_run_img']['day']) && !empty($_REQUEST['last_run_img']['month']) && !empty($_REQUEST['last_run_img']['year'])) {
    $date = $_REQUEST['last_run_img']['day'] . '/' . $_REQUEST['last_run_img']['month'] . '/' . $_REQUEST['last_run_img']['year'];
    $d = DateTime::createFromFormat('d/m/Y', $date);
    $d->setTime(0, 0, 0);
    $new_timestamp = $d->getTimestamp();
  }

  $last_run_date = date("m/d/Y", strtotime('-1 day', $new_timestamp));
  // get all images that have been updated since last run
  $filters = [];
  // CONSTANT FILTERS
  $filters['lhd'] = '[after ' . $last_run_date . ']'; // last edit date
  $filters['ff'] = 'jpg or jpeg or gif or png';

  $max_limit = $sync_stack;
  $start_img = 0;

  $result = widendam_api_v2_search_by_expression('', $start_img, $max_limit, TRUE, $filters);
  $num_results = $result->total_count;

  $prefilters = _kerrdental_widen_get_image_prefilters();

  // prepare array of operation for Batch API
  $operations = [
    [
      'kerrdental_widen_load_new_resource_assets',
      [$start_img, 100, $filters, $num_results, WIDEN_SYNC_TYPE_IMAGES]
    ],
    [
      'kerrdental_widen_sync_images',
      [$max_limit, $prefilters]
    ]
  ];

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_admin_sync_batch_api_sync_finished',
    'file' => drupal_get_path('module', 'kerrdental_widen') . '/kerrdental_widen.module',
    'title' => 'Widen sync images processing',
    'progress_message' => '',
  ));
}

function kerrdental_widen_admin_remove_expired_images($form, &$form_state) {
  $sync_stack = variable_get('kerrdental_widen_sync_images_stack', 10);

  if (!empty($form_state['values']['sync_stack'])) {
    $new_stack = intval($form_state['values']['sync_stack']);

    if ($new_stack !== $sync_stack) {
      $sync_stack = $new_stack;
      variable_set('kerrdental_widen_sync_images_stack', $new_stack);
    }
  }

  $new_timestamp = null;
  if (isset($_REQUEST['last_run_img']) && !empty($_REQUEST['last_run_img']['day']) && !empty($_REQUEST['last_run_img']['month']) && !empty($_REQUEST['last_run_img']['year'])) {
    $date = $_REQUEST['last_run_img']['day'] . '/' . $_REQUEST['last_run_img']['month'] . '/' . $_REQUEST['last_run_img']['year'];
    $d = DateTime::createFromFormat('d/m/Y', $date);
    $d->setTime(0, 0, 0);
    $new_timestamp = $d->getTimestamp();
  }

  $last_run_date = date("m/d/Y", strtotime('-1 day', $new_timestamp));
  // get all images that have been updated since last run
  $filters = [];
  // CONSTANT FILTERS
  $filters['lhd'] = '[after ' . $last_run_date . ']'; // last edit date
  $filters['ff'] = 'jpg or jpeg or gif or png';
  // PRE-FILTERS
  $prefilters = kerrdental_widen_prefilters_array();
  foreach ($prefilters as $prefilter) {
    if ($filter_terms = variable_get('kerrdental_widen_prefilters_images_' . $prefilter['key'])) {
      $type_filters = [];
      $terms = taxonomy_term_load_multiple($filter_terms);
      foreach ($terms as $t) {
        $type_filters[] = '{' . $t->name . '}';
      }
      $filters[$prefilter['key']] = implode(' or ', $type_filters);
    }
  }

  $max_limit = $sync_stack;
  $start_img = 0;

  $result = widendam_api_search_by_expression('', $start_img, $max_limit, TRUE, $filters);
  $num_results = $result->numResults;

  // prepare array of operation for Batch API
  $operations = [
    [
      'kerrdental_widen_sync_images',
      [$start_img, $max_limit, $filters, $num_results]
    ]
  ];

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_admin_sync_batch_api_sync_finished',
    'file' => drupal_get_path('module', 'kerrdental_widen') . '/kerrdental_widen.module',
    'title' => 'Widen sync images processing',
    'progress_message' => '',
  ));
}

function kerrdental_widen_delete_expired_last_month($form, &$form_state) {
  // get all assets that have been updated since last run
  $filters = [];

  cache_clear_all('kerrdental_widen:all_groups', 'cache');
  // CONSTANT FILTERS
  //    $filters['rd'] = '[after ' . variable_get('kerrdental_widen_remove_resources_cron_date', "11/01/2017") . ']'; // last run cron delete expired resources
  $filters['ed'] = '[' . date("m/d/Y", strtotime('-30 day')) . ' to ' . date("m/d/Y") . ']'; // expiration date

  $max_limit = 10;
  $start = 0;

  $result = widendam_api_v2_search_by_expression('', $start, $max_limit, TRUE, $filters);
  $num_results = $result->total_count;

  $prefilters = _kerrdental_widen_get_prefilters();
  // prepare array of operation for Batch API
  $operations = [
    [
      'kerrdental_widen_load_new_resource_assets',
      [$start, 100, $filters, $num_results, WIDEN_SYNC_TYPE_EXPIRED]
    ],
    [
      'kerrdental_widen_remove_expired_resource_assets',
      [$max_limit, $prefilters]
    ]
  ];

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_admin_sync_batch_api_sync_finished',
    'title' => 'Widen removing expired assets processing',
    'progress_message' => '',
  ));
}

function kerrdental_widen_admin_run_full_sync($form, &$form_state) {
  //prepare filters
  $filters = [];

  cache_clear_all('kerrdental_widen:all_groups', 'cache');
  // CONSTANT FILTERS
  $filters['rd'] = '[before ' . date("m/d/Y", strtotime('+1 day')) . ']'; // release date
  $filters['ed'] = '[after ' . date("m/d/Y", strtotime('-1 day')) . '] OR ed:isempty'; // expiration date

  // PRE-FILTERS
  $prefilters = kerrdental_widen_prefilters_array();
  foreach ($prefilters as $prefilter) {
    if ($filter_terms = variable_get('kerrdental_widen_prefilters_resources_' . $prefilter['key'])) {
      $type_filters = [];
      $terms = taxonomy_term_load_multiple($filter_terms);
      foreach ($terms as $t) {
        $type_filters[] = '{' . $t->name . '}';
      }
      $filters[$prefilter['key']] = implode(' or ', $type_filters);
    }
  }

  // check stack of batch
  if (!empty($form_state['values']['stack'])) {
    $max_limit = intval($form_state['values']['stack']);
    variable_set('kerrdental_widen_resource_full_sync_stack', $max_limit);
  } else {
    $max_limit = 2;
    variable_set('kerrdental_widen_resource_full_sync_stack', 2);
  }

  $start = 0;
  $results = widendam_api_v2_search_by_expression('', $start, $max_limit, TRUE, $filters);
  $num_results = $results->total_count;

  $prefilters = _kerrdental_widen_get_prefilters();

  // prepare array of operation for Batch API
  $operations = [
    [
      'kerrdental_widen_remove_old_assets',
      [WIDEN_SYNC_TYPE_FULL]
    ],
    [
      'kerrdental_widen_load_new_resource_assets',
      [$start, 50, $filters, $num_results, WIDEN_SYNC_TYPE_FULL]
    ],
    [
      'kerrdental_widen_remove_not_matched_resource_assets',
      []
    ],
    [
      'kerrdental_widen_full_sync_resource_assets',
      [$max_limit, $prefilters]
    ]
  ];

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_admin_sync_batch_api_sync_finished',
    'file' => drupal_get_path('module', 'kerrdental_widen') . '/kerrdental_widen.module',
    'title' => 'Widen Full sync assets processing',
    'progress_message' => '',
  ));
}

function kerrdental_widen_admin_save_settings_new_sync($form, &$form_state) {
  if (!empty($form_state['values']['new_stack'])) {
    $new_stack = intval($form_state['values']['new_stack']);
    variable_set('kerrdental_widen_resource_new_sync_stack', $new_stack);
  }
  if (!empty($form_state['values']['new_last_run'])) {
    $date = $form_state['values']['new_last_run']['day'] . '/' . $form_state['values']['new_last_run']['month'] . '/' . $form_state['values']['new_last_run']['year'];
    $d = DateTime::createFromFormat('d/m/Y', $date);
    $d->setTime(0, 0, 0);
    $new_timestamp = $d->getTimestamp();
    variable_set('kerrdental_widen_resource_new_sync_last_run', $new_timestamp);
  }
}

function kerrdental_widen_admin_run_new_sync($form, &$form_state) {
  $last_run_new_date = variable_get('kerrdental_widen_resource_new_sync_last_run', null);
  $last_run_new_stack = variable_get('kerrdental_widen_resource_new_sync_stack', 0);

  if (!empty($form_state['values']['new_stack'])) {
    $new_stack = intval($form_state['values']['new_stack']);

    if ($new_stack !== $last_run_new_stack) {
      $last_run_new_stack = $new_stack;
      variable_set('kerrdental_widen_resource_new_sync_stack', $new_stack);
    }
  }

  if (!empty($form_state['values']['new_last_run'])) {
    $date = $form_state['values']['new_last_run']['day'] . '/' . $form_state['values']['new_last_run']['month'] . '/' . $form_state['values']['new_last_run']['year'];
    $d = DateTime::createFromFormat('d/m/Y', $date);
    $d->setTime(0, 0, 0);
    $new_timestamp = $d->getTimestamp();

    if ($new_timestamp !== $last_run_new_date) {
      $last_run_new_date = $new_timestamp;
      variable_set('kerrdental_widen_resource_new_sync_last_run', $new_timestamp);
    }
  }

  // get all assets that have been updated since last run
  $filters = [];

  cache_clear_all('kerrdental_widen:all_groups', 'cache');
  // CONSTANT FILTERS
//  $filters['rd'] = '[before ' . date("m/d/Y", strtotime('+1 day')) . ']'; // release date
//  $filters['ed'] = '[after ' . date("m/d/Y", strtotime('-1 day')) . '] OR ed:isempty'; // expiration date
  if (null !== $last_run_new_date){
    $last_run_new_date_widen = date("m/d/Y", strtotime('-1 day', $last_run_new_date));
    $filters['lhd'] = '[after ' . $last_run_new_date_widen . ']'; // last edit date
  } else {
    $filters['lhd'] = '[after 01/01/2018]'; // last edit date
  }

  $max_limit_new = $last_run_new_stack;
  $start_new = 0;
  variable_set('kerrdental_widen_resource_new_sync_start', $start_new);

  $result_new = widendam_api_v2_search_by_expression('', $start_new, $max_limit_new, TRUE, $filters);
  $num_results_new = $result_new->total_count;

  $prefilters = _kerrdental_widen_get_prefilters();

  // prepare array of operation for Batch API
  $operations = [
    [
      'kerrdental_widen_load_new_resource_assets',
      [$start_new, 100, $filters, $num_results_new, WIDEN_SYNC_TYPE_NEW]
    ],
    [
      'kerrdental_widen_sync_new_resource_assets',
      [$max_limit_new, $prefilters]
    ]
  ];

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_admin_sync_batch_api_sync_finished',
    'file' => drupal_get_path('module', 'kerrdental_widen') . '/kerrdental_widen.module',
    'title' => 'Widen sync new assets processing',
    'progress_message' => '',
  ));
}

function kerrdental_widen_sync_domain_only($form, &$form_state) {
  $last_run_domain_stack = variable_get('kerrdental_widen_resource_domain_sync_stack', 10);

  if (!empty($form_state['values']['domain_stack'])) {
    $new_stack = intval($form_state['values']['domain_stack']);

    if ($new_stack !== $last_run_domain_stack) {
      $last_run_domain_stack = $new_stack;
      variable_set('kerrdental_widen_resource_domain_sync_stack', $new_stack);
    }
  }

  $time_start = microtime(TRUE);

  $results = db_select('node', 'n')
    ->fields('n', ['nid'])
    ->condition('n.type', 'resource_asset')
    ->execute();

  foreach ($results as $result) {
    $nids[] = $result->nid;
  }

  reset($nids);
  reset($assets_wc);
  $first_key = key($nids);
  $limit = $last_run_domain_stack;
  $domain_filters = variable_get('kerrdental_widen_prefilters_resources_domain', []);

  // prepare array of operation for Batch API
  $operations = [
    [
      'kerrdental_widen_sync_domain',
      [$first_key, $limit, $domain_filters, $nids]
    ]
  ];

  batch_set(array(
    'operations' => $operations,
    'finished' => 'kerrdental_widen_admin_sync_batch_api_sync_finished',
    'file' => drupal_get_path('module', 'kerrdental_widen') . '/kerrdental_widen.module',
    'title' => 'Widen Full sync domains assets processing',
    'progress_message' => '',
  ));
}

function kerrdental_widen_sync_nodeid($form, &$form_state) {
  $nids = explode(',', $form_state['values']['nodeid']);

  if (!empty($nids)) {
    global $base_url;
    $nodes = node_load_multiple($nids);

    if (!empty($nodes) && is_array($nodes)) {
      $prefilters = _kerrdental_widen_get_prefilters();
      foreach ($nodes as $node) {
        $uuid = $node->field_resource_uuid['und'][0]['value'];
        $parms = "?expand=asset_properties,file_properties,embeds,thumbnails,metadata,metadata_info,metadata_vocabulary,security";
        $asset = widendam_api_v2_call("assets/" . $uuid . $parms);
        kerrdental_widen_sync_resource_asset_processing($asset, $prefilters);
        $node = node_submit($node);
        node_save($node);

        $nodeurl = $base_url . "/" . drupal_get_path_alias("node/{$node->nid}");

        drupal_set_message("Done, check the <a href='$nodeurl' target='_blank'>{$node->nid} result</a>", $type = 'status');
      }
    }
  }
}

function kerrdental_widen_sync_uuid($form, &$form_state) {
  $uuid = $form_state['values']['uuid'];
  $parms = "expand=asset_properties,file_properties,embeds,thumbnails,metadata,metadata_info,metadata_vocabulary,security";
  $asset = widendam_api_v2_search_by_uuid($uuid, $parms);
  $prefilters = _kerrdental_widen_get_prefilters();
  kerrdental_widen_sync_resource_asset_processing($asset, $prefilters);

  $mgs = '';
  $err = 'status';
  // see if asset already exists - check widen_assets table
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'resource_asset')
    ->fieldCondition('field_resource_uuid', 'value', $asset->id);
  $result = $query->execute();
  if (isset($result['node'])) {
    foreach ($result['node'] as $id => $node) {
      $msg = 'Node ' . $id . ' was updated.';
    }
  } else { // new asset
    $msg = 'Asset doesn\'t match the prefilters or doesn\'t exists';
  }
  drupal_set_message($msg, $err);
}

function kerrdental_widen_admin_prefilters_form($form, &$form_state) {
  if (arg(4) == 'sync') {
    kerrdental_widen_sync_filters();
    drupal_goto('admin/kerrdental/widen/prefilters');
  }
  if (arg(4) == 'clean') {
    kerrdental_widen_check_assets();
    drupal_goto('admin/kerrdental/widen/prefilters');
  }

  $form = [];
  $form['tabs'] = [
    '#type' => 'vertical_tabs',
  ];

  // resource center
  $form['resource_center'] = [
    '#type' => 'fieldset',
    '#title' => t('Resource Center - Global'),
    '#collapsible' => TRUE,
    '#group' => 'tabs',
  ];
  $form['resource_center']['prefix'] = [
    '#type' => 'markup',
    '#markup' => '<div><strong>*Leave field blank to allow all values</strong></div>',
  ];
  $vocabularies = kerrdental_widen_prefilters_array();
  foreach ($vocabularies as $vid => $vocab) {
    $form['resource_center']['kerrdental_widen_prefilters_resources_' . $vocab['key']] = [
      '#title' => $vocab['name'],
      '#type' => 'select',
      '#options' => kerrdental_widen_taxonomy_to_select_options($vid),
      '#multiple' => TRUE,
      '#default_value' => variable_get('kerrdental_widen_prefilters_resources_' . $vocab['key'], ''),
    ];
  }

  if ( module_exists('domain') ) {
    // resource center domains
    $defaults = variable_get('kerrdental_widen_prefilters_resources_domain', []);
    $domains = domain_domains();
    foreach ($domains as $domain) {
      $form['resource_center_domain_' . $domain['domain_id']] = [
        '#type' => 'fieldset',
        '#title' => t('Resource Center - ' . $domain['sitename']),
        '#collapsible' => TRUE,
        '#group' => 'tabs',
      ];
      $form['resource_center_domain_' . $domain['domain_id']]['prefix'] = [
        '#type' => 'markup',
        '#markup' => '<div><strong>*Leave field blank to allow all values</strong></div>',
      ];
      $default_country = $default_language = [];
      if (isset($defaults[$domain['domain_id']]) && isset($defaults[$domain['domain_id']]['country'])) {
        $default_country = array_keys($defaults[$domain['domain_id']]['country']);
      }
      if (isset($defaults[$domain['domain_id']]) && isset($defaults[$domain['domain_id']]['language'])) {
        $default_language = array_keys($defaults[$domain['domain_id']]['language']);
      }
      $vocab = taxonomy_vocabulary_machine_name_load('widen_country');
      $form['resource_center_domain_' . $domain['domain_id']]['domain_' . $domain['domain_id'] . '_country'] = [
        '#title' => $vocab->name,
        '#type' => 'select',
        '#options' => kerrdental_widen_taxonomy_to_select_options($vocab->vid),
        '#multiple' => TRUE,
        '#default_value' => $default_country,
      ];
      $vocab = taxonomy_vocabulary_machine_name_load('widen_language');
      $form['resource_center_domain_' . $domain['domain_id']]['domain_' . $domain['domain_id'] . '_language'] = [
        '#title' => $vocab->name,
        '#type' => 'select',
        '#options' => kerrdental_widen_taxonomy_to_select_options($vocab->vid),
        '#multiple' => TRUE,
        '#default_value' => $default_language,
      ];
    }
  }

  // images
  $form['image'] = [
    '#type' => 'fieldset',
    '#title' => t('Image Search'),
    '#collapsible' => TRUE,
    '#group' => 'tabs',
  ];
  $form['image']['prefix'] = [
    '#type' => 'markup',
    '#markup' => '<div><strong>*Leave field blank to allow all values</strong></div>',
  ];
  $vocabularies = kerrdental_widen_prefilters_array();
  foreach ($vocabularies as $vid => $vocab) {
    $form['image']['kerrdental_widen_prefilters_images_' . $vocab['key']] = [
      '#title' => $vocab['name'],
      '#type' => 'select',
      '#options' => kerrdental_widen_taxonomy_to_select_options($vid),
      '#multiple' => TRUE,
      '#default_value' => variable_get('kerrdental_widen_prefilters_images_' . $vocab['key'], ''),
    ];
  }

  // sync link
  $form['sync'] = [
    '#type' => 'fieldset',
    '#title' => t('Sync'),
    '#collapsible' => TRUE,
    '#group' => 'tabs',
  ];
  $form['sync']['markup'] = [
    '#type' => 'markup',
    '#markup' => l('Sync Filters', 'admin/kerrdental/widen/prefilters/sync'),
  ];
  $form['sync']['devider'] = [
    '#type' => 'markup',
    '#markup' => '<br />'
  ];
  $form['sync']['markup1'] = [
    '#type' => 'markup',
    '#markup' => l('Remove not match assets', 'admin/kerrdental/widen/prefilters/clean'),
  ];

  // extra
  $form['suffix'] = [
    '#type' => 'markup',
    '#markup' => '<script type="text/javascript">
    (function($){
      $(document).ready(function(){
        $("#block-system-main select").each(function() {
          $(this).multiSelect({keepOrder:true});
        });
      });
    })(jQuery);
    </script>',
  ];
  $library = libraries_get_path('multi-select');
  $form['#attached'] = [
    'css' => [
      $library . '/css/multi-select.css',
    ],
    'js' => [
      $library . '/js/jquery.multi-select.js',
    ],
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Submit',
  ];
  return $form;
}

function kerrdental_widen_admin_prefilters_form_submit($form, &$form_state) {
  $vals = $form_state['values'];
  $domains = [];
  $vocab = taxonomy_vocabulary_machine_name_load('widen_country');
  $countries = kerrdental_widen_taxonomy_to_select_options_not_translated($vocab->vid);
  $vocab = taxonomy_vocabulary_machine_name_load('widen_language');
  $languages = kerrdental_widen_taxonomy_to_select_options_not_translated($vocab->vid);
  foreach ($vals as $key => $val) {
    // resources
    if (strpos($key, 'kerrdental_widen_prefilters_resources_') !== FALSE) {
      variable_set($key, $val);
    }
    // images
    if (strpos($key, 'kerrdental_widen_prefilters_images_') !== FALSE) {
      variable_set($key, $val);
    }
    if ( module_exists('domain') ) {
      // domains
      if (substr($key, 0, 7) == 'domain_') {
        $domain_type = str_replace('domain_', '', $key);
        list($domain_id, $type) = explode('_', $domain_type);
        if (!isset($domains[$domain_id])) {
          $domains[$domain_id] = [];
        }
        if (!isset($domains[$domain_id][$type])) {
          $domains[$domain_id][$type] = [];
        }
        foreach ($val as $term_id) {
          if ($type == 'country') {
            $domains[$domain_id][$type][$term_id] = $countries[$term_id];
          }
          elseif ($type == 'language') {
            $domains[$domain_id][$type][$term_id] = $languages[$term_id];
          }
        }
      }
    }
  }
  variable_set('kerrdental_widen_prefilters_resources_domain', $domains);
}
