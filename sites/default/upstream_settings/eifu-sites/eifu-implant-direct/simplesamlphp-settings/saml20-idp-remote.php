<?php

//PROD
if (isset($_ENV['PANTHEON_ENVIRONMENT']) && $_ENV['PANTHEON_ENVIRONMENT'] == 'live') {

  $metadata['https://sts.windows.net/137b3b98-f315-4025-ae27-3d17e9bf0d66/'] = array (
    'entityid' => 'https://sts.windows.net/137b3b98-f315-4025-ae27-3d17e9bf0d66/',
    'contacts' =>
      array (
      ),
    'metadata-set' => 'saml20-idp-remote',
    'SingleSignOnService' =>
      array (
        0 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
        1 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
      ),
    'SingleLogoutService' =>
      array (
        0 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
      ),
    'ArtifactResolutionService' =>
      array (
      ),
    'keys' =>
      array (
        0 =>
          array (
            'encryption' => false,
            'signing' => true,
            'type' => 'X509Certificate',
            'X509Certificate' => 'MIIC8DCCAdigAwIBAgIQOlN4x91v/otA1p69hL47rzANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMDAxMTQxMTE0MDdaFw0yMzAxMTQxMTE0MDdaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvqbYVQYH2yVfBNPip0IsfQREVxYRiL/+eL0Xqf0QF7xw/OB+icY453WjVEo3LKTHVhhYMr3AxlK9DUsivFr3InwprgOlUmbDtiElbwjJAHifxHJalSAeIycma+XTy+MmFnwngMXDn+GxmcweFXqcDd/gBz8EMeuuaM/0zEMtjeRigK/tXU+n1p8vOCqqBn89hTc4Wp0NQk+MMTUNATofYsfBBlqTeIrcNN97lE73h5S/xDlsI7trfuSoOjgGA6lhBIAxmRt2nIbrrznreSN87tEqZouL+F3qflagk+GwbOwYFlxVsDtM2Y1atb9tHQLKK2IBUEDONm7fMNaP8YwqvQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCmMCZYtkZn3uagPnzIbI+dZJCr15H7BjDXMy7dEz2+yTuMqFWdxSZhnVXOL96+qNHqDyCXCwkk6wfkzNeN3C8+jjt89Ie+3odXQB9eadBlcOrA27rtHcj2PY9l1AIM9+rJTqjuCMacaqPZkq8qtEmp64z5px/StpF5OCYksija2FljL7aib/43sFGhm7b7nZVAhCYtrqsgu88FfUNKv+wgr25xexFAkZWAehFDvYiGV7EvEoyHj2fz4IPUZCG8SDKKCgZsHeNEwVYCxeWdp99TaS5E3Rp7Gwl+rLgBXW6aDQ3aMbNPBsYsAc51fTcuJH9tl0FdEdwa6VdtgxMkPqJ/',
          ),
      ),
  );

} elseif (isset($_ENV['PANTHEON_ENVIRONMENT']) && $_ENV['PANTHEON_ENVIRONMENT'] == 'test') {

  $metadata['https://sts.windows.net/137b3b98-f315-4025-ae27-3d17e9bf0d66/'] = array (
    'entityid' => 'https://sts.windows.net/137b3b98-f315-4025-ae27-3d17e9bf0d66/',
    'contacts' =>
      array (
      ),
    'metadata-set' => 'saml20-idp-remote',
    'SingleSignOnService' =>
      array (
        0 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
        1 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
      ),
    'SingleLogoutService' =>
      array (
        0 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
      ),
    'ArtifactResolutionService' =>
      array (
      ),
    'keys' =>
      array (
        0 =>
          array (
            'encryption' => false,
            'signing' => true,
            'type' => 'X509Certificate',
            'X509Certificate' => 'MIIC8DCCAdigAwIBAgIQHPFDH+fGma5B7BQyXFOZPTANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMDAxMTQxMTIwMDVaFw0yMzAxMTQxMTIwMDVaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvMF3XYNLBIv9g7Sv9JH5QJP54Y+04ufC15RdfzYuX7gNcm5XnZ9l9++nQncpR5KDFpfG8qZa1PLSwbVtpulcTxCUr51GfrcILpSSzQ+z4klr6Tm5x5SD/sw/9tX8qiYy8kctyKB8jf2UpH1sCI9F40pmxCQXScmD1B8k5YaHzsR4ShaeQBkGvjm53oZ9Xx98pv9Jv5cu6UYduT3yIZcz8RtW6zeWL5KIYUwlX9vr2zLtsFYqXPxMMK2kQapPAC0AAw4I4jcQdayQynLBimSeE6nrAwo4bSaiEDE5Bl7cky+fhoOflOTS8y6ZroHfxRqsoeXa8r2ViPN6Il3opLXFQwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAvr/aEw1iOfjy8KG3a3/5VdiRtktuY7ZeRBV8bj1z4B4qI65cDffYz0t6ScdxIFeLA97lqIwmKrKSA6svH8ISmsuR+KKPQ5L4Uirh5k0i7TnOsMy+aueWhT7yORWTfU3mlAwUIoDITTdpWGeJE9SFiYGGeL4Jnljhgi7RNNiZ8+bNTqTw8GDuh2p56LdFCdD4wE1Qmc/rUh7hWIyCU2FH0k+ZHtul7R8RwIopv1/pQQcPehINxkjOOchmOpMok9Ew94xl3XBsfx/lAJa1zie+pD+SKOkuy4HdHgDK5IoR7yta4uyaGTElmY7kT8JVFxTILMEG47fjHfZu/p/PVfOWp',
          ),
      ),
  );

} elseif (isset($_ENV['PANTHEON_ENVIRONMENT']) && $_ENV['PANTHEON_ENVIRONMENT'] == 'dev') {

  $metadata['https://sts.windows.net/137b3b98-f315-4025-ae27-3d17e9bf0d66/'] = array (
    'entityid' => 'https://sts.windows.net/137b3b98-f315-4025-ae27-3d17e9bf0d66/',
    'contacts' =>
      array (
      ),
    'metadata-set' => 'saml20-idp-remote',
    'SingleSignOnService' =>
      array (
        0 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
        1 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
      ),
    'SingleLogoutService' =>
      array (
        0 =>
          array (
            'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
            'Location' => 'https://login.microsoftonline.com/137b3b98-f315-4025-ae27-3d17e9bf0d66/saml2',
          ),
      ),
    'ArtifactResolutionService' =>
      array (
      ),
    'keys' =>
      array (
        0 =>
          array (
            'encryption' => false,
            'signing' => true,
            'type' => 'X509Certificate',
            'X509Certificate' => 'MIIC8DCCAdigAwIBAgIQLKsbc1Flv7hMAmHbWbOzqDANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMDAxMTQxMDE1MDVaFw0yMzAxMTQxMDE1MDVaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5HwDmyluyg50VWv0jnUDjWsKVO0xJANj8Cc1eqsKbdQR2MbcZqVcXztU6n6K3+t7My5MMjspFWdwek0u9EA1/cOi9GJN7tt3+UPntE5eZ3v4t3fExDu6e0d94DrAQaeb210MJ3Bh26ftaMM5uHCdlt8g6FCyvW47wTFgXThi+PNk7jiay/9JIDbRGo6pYrHvJzsorS5gp0qz+qYvPejCUwcR4muMCUr/2O44p0aWyeDwHvrhoMRWLrrZliYGvKX84WocvAc4SXr++87j6R3KfnVFYPTE9TkSDXRwFsfYqfJYdiXIS4etbxaaXbpA4TZ14ofQON+aCTCt+qgLlIAw0QIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAc4GSclitQx2itRsp1PI+GOPpDhFOj8I86y0xrDZGrOT58TPf5t5KWFsNKTDs0JSwOOKsMqjYetxNMmvgPP5WnMQuH5phaWCESc/y0dnY7kmdd6Bfd4vhCcnvVF0LovqJV8UPdPcvGJo/L6hh83LvSX1TVH9jVjuWs/xBMBs+C37UEALIwgvFtvfDgtYDxXN1yohYj6hyd/9eBx9Jip7QuKHXeN9xpnzvcC0V7JbMFeMiQtI0QkpvXUjhdQXymncrGv0Mb5D2wesn1j3Oi7umg82pkYZz6IZNCSQq6ZF3muxjAWD/SgZb1xaPak1T+BtDEzryqBsNsxX4YGlehVK7E',
          ),
      ),
  );

}
